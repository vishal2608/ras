<?php

use Illuminate\Database\Seeder;

use App\Role;

class RoleSeeder extends Seeder
{
   
    public function run()
    {

        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        Role::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');


        $role = Role::create(//1
            [
                'name' => 'SU',
                'label' => 'Super User'
            ]);


        $role->permissions()->sync(\App\Permission::pluck('id'));


        $arr = [

            [
                'name' => 'OP',
                'label' => 'Operator'
            ],
            [
                'name' => 'SO',
                'label' => 'Site Operator'
            ],
            [
                'name' => 'CM',
                'label' => 'Customer Operator'
            ],
            [
                'name' => 'SLO',
                'label' => 'Supplier Operator'
            ],
            [
                'name' => 'SLA',
                'label' => 'Supplier Approver'
            ],
            [
                'name' => 'BANK',
                'label' => 'Bank Operator'
            ],
        ];

        foreach ($arr as $a) {
            Role::create($a);
        }


    }
}
