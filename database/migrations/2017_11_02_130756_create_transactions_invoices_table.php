<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions_invoices', function (Blueprint $table) {
            $table->increments('id');
            $table->string('transaction_number',255)->comment = "Unique number for bunch of selected invoice";
            $table->integer('invoice_id')->unsigned();

            $table->integer('customer_id')->unsigned()->comment = "customer id";
            $table->integer('supplier_id')->unsigned()->comment = "supplier id";

            $table->string('payment_type')->nullable()->default(null);



            $table->integer('approver_id')->nullable()->default(0);
            $table->integer('requester_id')->nullable()->default(0);

            $table->date('anticipation_date')->nullable()->comment = "anticipation_date";

            $table->integer('bank_id')->nullable()->default(0);
            $table->integer('bank_contacts_id')->nullable()->default(0);
            $table->integer('bank_accounts_id')->nullable()->default(0);

            $table->string('payment_status')->nullable()->default(null)->comment = "paid , cancelled , cancelby , expired";

            $table->integer('created_by')->nullable()->default(0);
            $table->integer('updated_by')->nullable()->default(0);

            $table->timestamps();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions_invoices');
    }
}
