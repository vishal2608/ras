<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateActionLog extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('actions_logs', function (Blueprint $table) {
            //
            $table->string('tablename')->nullable();
            $table->integer('field_id')->nullable();
            $table->string('role')->nullable();
            $table->string('url')->nullable();
            $table->string('ip')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('actions_logs', function (Blueprint $table) {
            //
            $table->dropColumn('tablename');
            $table->dropColumn('field_id');
            $table->dropColumn('role');
            $table->dropColumn('url');
            $table->dropColumn('id');

        });
    }
}
