<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsInTransaction extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transactions', function (Blueprint $table) {
            //
            $table->float('total_amount')->default(0);
            $table->float('discount_rate')->default(0);
            $table->float('negotiate_rate')->default(0);
            $table->float('rate_class_min')->default(0);
            $table->float('bank_rate')->default(0);
            $table->float('discount_value')->default(0);
            $table->float('discounted_value')->default(0);
            $table->float('cost')->default(0);
            $table->float('net_value')->default(0);
            $table->integer('bank_id')->default(0);
            $table->string('type')->nullable();
            $table->integer('approver_id')->default(0);
            $table->integer('requester_id')->default(0);
            $table->string('payment_status')->nullable();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transactions', function (Blueprint $table) {
            //
            $table->dropColumn('total_amount');
            $table->dropColumn('discount_rate');
            $table->dropColumn('negotiate_rate');
            $table->dropColumn('rate_class_min');
            $table->dropColumn('bank_rate');
            $table->dropColumn('discount_value');
            $table->dropColumn('discounted_value');
            $table->dropColumn('cost');
            $table->dropColumn('net_value');
            $table->dropColumn('bank_id');
            $table->dropColumn('type');
            $table->dropColumn('approver_id');
            $table->dropColumn('requester_id');
            $table->dropColumn('payment_status');
        });
    }
}
