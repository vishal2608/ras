<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldToCustomerSupplierTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customer_supplier', function (Blueprint $table) {
            $table->string('customer_supplier_rate')->nullable();
            $table->string('customer_supplier_rate_by_commercial_user')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customer_supplier', function (Blueprint $table) {
            $table->dropColumn('customer_supplier_rate');
            $table->dropColumn('customer_supplier_rate_by_commercial_user');
        });
    }
}
