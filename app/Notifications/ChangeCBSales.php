<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class ChangeCBSales extends Notification
{
    use Queueable;

    public $sales;
    public $old_sales;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($customer_users,$old_user)
    {
        //
        $this->sales = $customer_users;
        $this->old_sales = $old_user;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
            'customer_id' => $this->sales->customer_id,
            'new_sales_user_id' => $this->sales->user_id,
            'new_sales_user_name' => $this->sales->user->name,
            'customer_name' => $this->sales->customer->name,
            'changed_by' => \Auth::user()->name,
            'old_sales_user_id' => $this->old_sales->id,
            'old_sales_user_name' => $this->old_sales->name,
        ];
    }
}
