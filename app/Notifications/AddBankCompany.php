<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class AddBankCompany extends Notification
{
    use Queueable;

    public $bank_cust;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($bank_customer)
    {
        //
        $this->bank_cust = $bank_customer;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
            'bank_customer_id' => $this->bank_cust->id,
            'bank_id' => $this->bank_cust->bank_id,
            'customer_id' => $this->bank_cust->customer_id,
            'bank_name' => $this->bank_cust->bank->name,
            'customer_name' => $this->bank_cust->customer->name,
            'amount' => $this->bank_cust->total_amount,
            'date' => $this->bank_cust->date,
        ];
    }
}
