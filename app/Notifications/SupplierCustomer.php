<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class SupplierCustomer extends Notification
{
    use Queueable;

    public $cust_supp;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($cust_sup)
    {
        //
        $this->cust_supp = $cust_sup;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {   

        return [
            //
            'customer_id' => $this->cust_supp->customer_id,
            'supplier_id' => $this->cust_supp->supplier_id,
            'customer_name' => $this->cust_supp->customer->name,
            'supplier_name' => $this->cust_supp->supplier->name,
        ];
    }
}
