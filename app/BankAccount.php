<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BankAccount extends Model
{
    //
    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'bank_accounts';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['bank_id','account_number','account_info','agency_number','agency_name','info', 'street_name','address_number','complement','neighborhood','country','state','city','zip','branch','deleted_at'];

    public function bank(){
        return $this->belongsTo('App\Bank');
    }

    public function city(){
            return $this->belongsTo('App\City','city');
        }

    
}
