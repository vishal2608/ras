<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BankCustomerMoney extends Model
{
    //
    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'bank_customer_allocated_money';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['bank_id','customer_id','total_amount','date','duedate','type','created_by','updated_by','deleted_at','bank_rate'];


    public function customer(){
        return $this->belongsTo('App\Customer','customer_id');
    }

    public function bank(){
        return $this->belongsTo('App\Bank');
    }

    public function AvailableCashHistory(){
        return $this->hasMany('App\AvailableCashHistory','customer_id','customer_id');
    }

}
