<?php

namespace App;

use App\Scope\GlobalWebSiteScope;
use Illuminate\Database\Eloquent\Model;

use Auth;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\BaseModelTrait;

class BaseModel extends Model
{
    use SoftDeletes,BaseModelTrait;
}
