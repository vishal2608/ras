<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
class Transaction extends Model
{

    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'transactions';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['customer_id','deleted_at','schedule_pay_day','cancelled_by','cancelled_date','supp_bank'];

    public function customer(){
        return $this->belongsTo('App\Customer','customer_id');
    }

    public function cessionAmount(){
        return $this->belongsToMany('App\Invoice', 'transactions_invoices', 'transaction_number', 'invoice_id')->select('type', DB::raw('sum(invoices.total_amount) as invoicesum ,count(invoices.id) as anticipation_invoice') )
            ->where('type', 'Cession')
            ->where('transactions_invoices.approver_id', '>', '0')
            ->groupBy('transactions_invoices.supplier_id');
    }

    public function anticipationAmount(){
        return $this->belongsToMany('App\Invoice', 'transactions_invoices', 'transaction_number', 'invoice_id')->select('type', DB::raw('sum(invoices.total_amount) as invoicesum ,count(invoices.id) as anticipation_invoice','transactions_invoices.supplier_id') )
            ->where('type', 'Anticipation')
            ->where('transactions_invoices.approver_id', '>', '0')
            ->groupBy('transactions_invoices.supplier_id');
    }

    public function totalAmount(){
        return $this->belongsToMany('App\Invoice', 'transactions_invoices', 'transaction_number', 'invoice_id')->select(DB::raw('sum(invoices.total_amount) as invoicesum, avg(invoices.discount_rate) as discount_rate, avg(invoices.cost) as bank_cost ,count(invoices.id) as anticipation_invoice,requester.name as requester_name, approver.name as approver_name,transactions_invoices.payment_status as payment_status') )
            ->leftJoin('users as requester', 'requester.id','=', 'transactions_invoices.requester_id')
            ->leftJoin('users as approver', 'approver.id', '=', 'transactions_invoices.approver_id')
            ->groupBy('transactions_invoices.transaction_number');
    }
    
    public function total_invoice(){
        return $this->belongsToMany('App\Invoice', 'transactions_invoices', 'transaction_number', 'invoice_id')->select(DB::raw('count(invoices.id) as total'), DB::raw('sum(invoices.total_amount) as invoicesum'))
        ->groupBy('transactions_invoices.transaction_number');
    }

    public function requester(){
        return $this->belongsTo('App\User','requester_id');
    }
    public function approver(){
        return $this->belongsTo('App\User','approver_id');
    }
    public function canceller(){
        return $this->belongsTo('App\User','cancelled_by');
    }

    public function bank(){
        return $this->belongsTo('App\Bank','bank_id'); 
    }

    public function supplier_bank(){
        return $this->belongsTo('App\SupplierBank','supp_bank'); 
    }

    public function invoices(){
        return $this->belongsToMany('App\Invoice', 'transactions_invoices', 'transaction_number', 'invoice_id');
    }

    public function supplier(){
        return $this->belongsTo('App\Supplier','supplier_id');
    }

    public function customerCession()
    {
        return $this->belongsTo('App\Customer', 'customer_id');
    }
    public function supplierName(){
        return $this->belongsTo('App\Supplier','supplier_id')->select('*','supplier.name as supplier_name');
    }
    public function supplierunitName(){
        return $this->belongsToMany('App\Invoice','transactions_invoices','transaction_number')
          -> Join('supplier_unit','supplier_unit.id','=', 'invoices.supplier_unit_id')->select('*');
    }
     public function bankDetails(){
        return $this->hasOne('App\BankAccount','bank_id','bank_id');
    }
     public function bankName(){
        return $this->hasOne('App\Bank','id','bank_id');
    }
     public function monthlyReports(){
         return $this->hasMany('App\TransactionsInvoices', 'transaction_number')   
         -> join('invoices','invoices.id','=','transactions_invoices.invoice_id')
         -> join('customer','customer.id','=','transactions_invoices.customer_id')
         -> select('*',DB::raw('sum(invoices.total_amount) AS original_value'),DB::raw('sum(invoices.discount_value) AS discount_val'),DB::raw('sum(invoices.cost) AS cost_val'),DB::raw('count(transactions_invoices.transaction_number) AS total_transaction'),'transactions_invoices.payment_type as payment_type','transactions_invoices.transaction_number as transaction_number','transactions_invoices.transaction_number as transaction_number','invoices.total_amount as total_amount','customer.rate as rate');
    }

     public function monthlydueReports(){
         return $this->hasMany('App\TransactionsInvoices', 'transaction_number')   
         -> join('invoices','invoices.id','=','transactions_invoices.invoice_id')
          -> join('customer','customer.id','=','transactions_invoices.customer_id')
         -> select('*',DB::raw('sum(invoices.total_amount) AS original_value'),'transactions_invoices.payment_type as payment_type','transactions_invoices.transaction_number as transaction_number','invoices.total_amount as total_amount');
    }

    public function getCreatedBy(){
        return $this->belongsTo('App\User','requester_id')->with('role');
    }

    public function getCustomerRoles(){
        return $this->belongsTo('App\CustomerUser','customer_id','customer_id')
        ->leftJoin('users','users.id','=','customer_user.user_id')
        ->leftJoin('role_user','role_user.user_id','=','users.id')
        ->leftJoin('roles','roles.id','=','role_user.role_id')
        ->where('roles.name','=','CBS')
        ->select('*','users.name as user_name');
    }
    public function getSupplierRoles(){
        return $this->belongsTo('App\SupplierUser','supplier_id','supplier_id')
        ->leftJoin('users','users.id','=','supplier_user.user_id')
        ->leftJoin('role_user','role_user.user_id','=','users.id')
        ->leftJoin('roles','roles.id','=','role_user.role_id')
        ->where('roles.name','=','CBC')
        ->select('*','users.name as user_name');
    }
	public function getCustomerApproveTransactionRoles(){
        return $this->hasMany('App\CustomerUser','customer_id','customer_id')
       ->leftJoin('users','users.id','=','customer_user.user_id')
       ->leftJoin('role_user','role_user.user_id','=','users.id')
	   ->leftJoin('customer','customer.id','=','customer_user.customer_id')
       ->leftJoin('roles','roles.id','=','role_user.role_id')
		->orWhereIn('roles.name',array('CM','CBS'))
        ->select('*','users.name as user_name','customer.name as customer_name');
    }
    public function getSupplierApproveTransactionRoles(){
       return $this->hasMany('App\SupplierUser','supplier_id','supplier_id')
        ->leftJoin('users','users.id','=','supplier_user.user_id')
        ->leftJoin('role_user','role_user.user_id','=','users.id')
		->leftJoin('supplier','supplier.id','=','supplier_user.supplier_id')
        ->leftJoin('roles','roles.id','=','role_user.role_id')
		->orWhereIn('roles.name',array('SLA','CBC'))
        ->select('*','users.name as user_name','supplier.name as supplier_name');
    }
    
}
