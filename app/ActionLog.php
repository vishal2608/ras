<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ActionLog extends Model
{
    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'actions_logs';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['actioner_id','actioner_name','action','detail','created_by','updated_by','tablename','field_id','role','url','ip','sub_detail','deleted_at'];

    public function company(){

        return $this->belongsTo('App\User','actioner_id')
            ->select('users.*','customer.name as customer_name','roles.name as role_name','roles.id as role_id','roles.label as role_label','supplier.name as supplier_name')
            ->join('role_user', 'users.id', '=', 'role_user.user_id')
            ->join('roles', 'roles.id', '=', 'role_user.role_id')
            ->leftJoin('customer_user', 'customer_user.user_id', '=', 'users.id')
            ->leftJoin('customer', 'customer.id', '=', 'customer_user.customer_id')
            ->leftJoin('supplier_user', 'supplier_user.user_id', '=', 'users.id')
            ->leftJoin('supplier', 'supplier.id', '=', 'supplier_user.supplier_id');
    }

}
