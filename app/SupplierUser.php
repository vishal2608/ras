<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SupplierUser extends Model
{
    //Tablename
    protected $table = "supplier_user";
    //primary key
    public $primaryKey = "id";

    public function user(){
        return $this->belongsTo('App\User','user_id');
    }

    public function supplier(){
        return $this->belongsTo('App\Supplier','supplier_id');
    }
}
