<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class CustomerSupplier extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'customer_supplier';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';
    
    public function supplier(){
        return $this->belongsTo('App\Supplier','supplier_id','id')->with('unit');
    }

    public function customer(){
        return $this->belongsTo('App\Customer','customer_id','id');
    }

    public function discountRate(){
        return $this->belongsTo('App\DiscountRate','customer_supplier_rate_by_commercial_user');
    }

    public function discountRateCustomer(){
        return $this->belongsTo('App\DiscountRate', 'customer_supplier_rate');
    }

    public function cashAvailable(){

        return $this->hasMany('App\AvailableCashHistory','customer_id','customer_id')
            ->select(DB::raw('sum(available_cash_history.amount) as available_cash,available_cash_history.customer_id'))
            ->whereNull('bank_id')
            ->groupBy('available_cash_history.customer_id');

    }

    public function bank_cost(){
        return $this->hasMany('App\BankCustomerMoney', 'customer_id','customer_id');

    }
    public function customer_discount_rate(){
        return $this->belongsTo('App\DiscountRate','customer_supplier_rate');
    }


}
