<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SupplierBank extends Model
{
    //
     /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'supplier_bank';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['supplier_id','bank_name','bank_number','agency_name','agency_number','account_number','account_info','info','status'];

    public function supplier(){

        return $this->belongsTo('App\Supplier');
    }
}
