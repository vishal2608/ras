<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth;

use Closure;
use App\Setting;
use App\Hollyday;
use Carbon\Carbon;

class SystemTime
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
		
		$setting1 = Setting::where("key","switch_system")->first();
		
		if($setting1 && $setting1->value ==0){ 
			return redirect('admin/system-off');
		} 
		
		$day_key ="";
		if(Carbon::now()->dayOfWeek==1){ $day_key ="working_day_mon"; }
		if(Carbon::now()->dayOfWeek==2){ $day_key ="working_day_tue"; }
		if(Carbon::now()->dayOfWeek==3){ $day_key ="working_day_wed"; }
		if(Carbon::now()->dayOfWeek==4){ $day_key ="working_day_thu"; }
		if(Carbon::now()->dayOfWeek==5){ $day_key ="working_day_fri"; }
		if(Carbon::now()->dayOfWeek==6){ $day_key ="working_day_sat"; }
		if(Carbon::now()->dayOfWeek==7){ $day_key ="working_day_sun"; }
       
		$setting1 = Setting::where("key",$day_key)->first();
                if($setting1 && $setting1->value ==0){ 
			return redirect('admin/system-off');
		}
		
		$stime = Setting::where("key","working_start_time")->first();
		$etime = Setting::where("key","working_end_time")->first();
		
                $today = Carbon::now()->format("Y-m-d");
		if($stime && $etime){
			
			$start_time = Carbon::createFromFormat('Y-m-d H:i:s',$today." ".$stime->value.":00");
			$end_time = Carbon::createFromFormat('Y-m-d H:i:s',$today." ".$etime->value.":00");
			
			if(Carbon::now()->format("Y-m-d H:i:s") >= $start_time && Carbon::now()->format("Y-m-d H:i:s")<=$end_time){
				
			}else{
				return redirect('admin/system-off');
			}
		}
                
                $holiday = Hollyday::where("holiday_date",$today)->first();
                if($holiday){
                    return redirect('admin/system-off');
                }
	   
       return $next($request);
    }
}
