<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Session;
use App\Customer;
use App\Transaction;
use App\TransactionsInvoices;
use Carbon\Carbon;
use Illuminate\Support\Facades\Redirect;
class MonthyreportsController extends Controller
{
     public function index(Request $request)
    {
        $customer = Customer::pluck('name','id');
           
            if(!empty($request->name)) {
          
                $cust_id=$request->name;
                /*convert  month and year using Carbon*/

              $startmonth =  Carbon::createFromFormat('d-m-Y H:s:i',"1-". $request->startdate." 00:00:00");
              $endmonth =  Carbon::createFromFormat('d-m-Y H:s:i',"31-". $request->enddate." 23:59:59");
              
                $dailyupdate = Transaction:: where('transactions.customer_id',$cust_id)
                -> whereBetween('transactions.updated_at',[$startmonth, $endmonth])
                ->where('transactions.is_approve','=','1')
                ->where('transactions.customer_status','=','1')->orderBy('transactions.updated_at','asc') 
                ->select('*','transactions.id as transaction_id',DB::raw('DATE(transactions.updated_at) AS updates_date'))
                ->with('monthlyReports')
                ->get();
                $i=0;  
                $finalArra =''; 
                if(count($dailyupdate)>0){
                    foreach($dailyupdate as  $rows){

                        foreach($rows->monthlyReports as $transactiondata){
                            $finalArra[$i]['updated_at'] = $request->startdate;
                            $finalArra[$i]['total_transaction'] = $transactiondata->total_transaction;
                            $finalArra[$i]['original_value'] = $transactiondata->total_amount;              
                            $finalArra[$i]['net_val'] = $transactiondata->net_value;
                            $finalArra[$i]['cost_val'] = $transactiondata->cost;
                            $total =  $transactiondata->total_amount-($transactiondata->net_value)-($transactiondata->cost);
                            $finalArra[$i]['total']=$total;
                            $finalArra[$i]['revenue'] = $total*$transactiondata->rate;
                            $i++;
                        }  
                    }

                    if ( count($finalArra) > 0) {
            
                        $filename = "file.csv";
                        $handle = fopen($filename, 'w+');
                        $data = array('Month','Transactions','Original Value','Net Value','Cost','Total','revenue');

                        fputcsv($handle, $data);

                        foreach($finalArra as $value) {
                            fputcsv($handle, $value);
                        }
                        fclose($handle);

                        $headers = array(
                            'Content-Type' => 'text/csv',
                        );
                        return response()->download($filename, 'monthly_reports '.date("d-m-Y H:i").'.csv', $headers);
                    }
                    else{
                       // Session::flash('flash_warning', __('Data is not Available!')); 
                      //  return Redirect::back();
                    }
            
                }else{
                   Session::flash('flash_warning', __('Data is not Available!')); 
                    return Redirect::back();
                }
            }

            return view('admin.monthlyreports.index',compact('customer'));
    }
}
