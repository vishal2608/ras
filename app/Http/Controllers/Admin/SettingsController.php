<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Permission;
use App\Role;
use Illuminate\Http\Request;
use Session;
use App\Setting;
use App\Hollyday;

use Dedicated\GoogleTranslate\Translator;

class SettingsController extends Controller
{   
    public function __construct()
    {
        $this->middleware('permission:access.systemsetting');
    }


    public function index()
    {
        $settings = Setting::all();
        
		$setting=[
			"switch_system"=>null,
			"working_start_time"=>null,
			"working_end_time"=>null,
			"working_day_mon"=>1,
			"working_day_tue"=>1,
			"working_day_wed"=>1,
			"working_day_thu"=>1,
			"working_day_fri"=>1,
			"working_day_sat"=>1,
			"working_day_sun"=>1,
			
		];
        if($settings){
			foreach($settings as $s){
				if($s->key =="switch_system") { $setting['switch_system'] = $s->value; }
				if($s->key =="working_start_time") { $setting['working_start_time'] = $s->value; }
				if($s->key =="working_end_time") { $setting['working_end_time'] = $s->value; }
				if($s->key =="working_day_mon") { $setting['working_day_mon'] = $s->value; }
				if($s->key =="working_day_tue") { $setting['working_day_tue'] = $s->value; }
				if($s->key =="working_day_wed") { $setting['working_day_wed'] = $s->value; }
				if($s->key =="working_day_thu") { $setting['working_day_thu'] = $s->value; }
				if($s->key =="working_day_fri") { $setting['working_day_fri'] = $s->value; }
				if($s->key =="working_day_sat") { $setting['working_day_sat'] = $s->value; }
				if($s->key =="working_day_sun") { $setting['working_day_sun'] = $s->value; }
			}
        }

        $hollydays = Hollyday::all();
        
        return view('admin.settings.system-settings',compact('setting','hollydays'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
		//echo "<pre>"; print_r($request->all()); exit;
        $setting1 = Setting::where("key","switch_system")->first();
        if(!$setting1){ $setting1 = new Setting(); $setting1->key ="switch_system"; } 
        if($request->has('switch_system')){
            $setting1->value = 1;
        }else{
            $setting1->value = 0;
        }
        $setting1->save();
        
        
        $setting2 = Setting::where("key","working_start_time")->first();
        if(!$setting2){ $setting2 = new Setting(); $setting2->key ="working_start_time"; } 
        if($request->has('working_start_time')){
            $setting2->value = $request->working_start_time;
        }
        $setting2->save();
        
        
        $setting3 = Setting::where("key","working_end_time")->first();
        if(!$setting3){ $setting3 = new Setting(); $setting3->key ="working_end_time"; } 
        if($request->has('working_end_time')){
            $setting3->value = $request->working_end_time;
        }
        $setting3->save();
		
		
		//working_day_mon
		$setting4 = Setting::where("key","working_day_mon")->first();
        if(!$setting4){ $setting4 = new Setting(); $setting4->key ="working_day_mon"; } 
        if($request->has('working_day_mon')){
            $setting4->value = 1;
        }else{
            $setting4->value = 0;
        }
        $setting4->save();
		
		//working_day_tue
		$setting5 = Setting::where("key","working_day_tue")->first();
        if(!$setting5){ $setting5 = new Setting(); $setting5->key ="working_day_tue"; } 
        if($request->has('working_day_tue')){
            $setting5->value = 1;
        }else{
            $setting5->value = 0;
        }
        $setting5->save();
		
		
		//working_day_wed
		$setting6 = Setting::where("key","working_day_wed")->first();
        if(!$setting6){ $setting6 = new Setting(); $setting6->key ="working_day_wed"; } 
        if($request->has('working_day_wed')){
            $setting6->value = 1;
        }else{
            $setting6->value = 0;
        }
        $setting6->save();
		
		//working_day_thu
		$setting7 = Setting::where("key","working_day_thu")->first();
        if(!$setting7){ $setting7 = new Setting(); $setting7->key ="working_day_thu"; } 
        if($request->has('working_day_thu')){
            $setting7->value = 1;
        }else{
            $setting7->value = 0;
        }
        $setting7->save();
		
		//working_day_fri
		$setting8 = Setting::where("key","working_day_fri")->first();
        if(!$setting8){ $setting8 = new Setting(); $setting8->key ="working_day_fri"; } 
        if($request->has('working_day_fri')){
            $setting8->value = 1;
        }else{
            $setting8->value = 0;
        }
        $setting8->save();
		//working_day_sat
		$setting9 = Setting::where("key","working_day_sat")->first();
        if(!$setting9){ $setting9 = new Setting(); $setting9->key ="working_day_sat"; } 
        if($request->has('working_day_sat')){
            $setting9->value = 1;
        }else{
            $setting9->value = 0;
        }
        $setting9->save();
		// working_day_sun
		$setting10 = Setting::where("key","working_day_sun")->first();
        if(!$setting10){ $setting10 = new Setting(); $setting10->key ="working_day_sun"; } 
        if($request->has('working_day_sun')){
            $setting10->value = 1;
        }else{
            $setting10->value = 0;
        }
        $setting10->save();
		
        Session::flash('flash_success',"System Settings Saved Success !!");
		return redirect()->back();
        
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
	public function systemOff()
    {
        return view('admin.system-off');
    }
}