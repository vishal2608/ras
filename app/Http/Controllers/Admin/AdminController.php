<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Permission;
use App\Role;
use Illuminate\Http\Request;
use Session;

use Dedicated\GoogleTranslate\Translator;

class AdminController extends Controller
{

    public function index()
    {
       if (!Auth::user()){
            return redirect('login');
       }
      
       if(Auth::user()->roles[0]->name=='SU'){
             return redirect('/admin/dashboard');
       }else if(Auth::user()->roles[0]->name=='CBS'){
             return redirect('/admin/salesdashboard');
       }else if(Auth::user()->roles[0]->name=='CBC'){
             return redirect('/admin/commercialdashboard');
       }else if(Auth::user()->roles[0]->name=='CBB'){
             return redirect('/admin/backofficedashboard');
       }else if(Auth::user()->roles[0]->name=='SLO'){
             return redirect('/admin/supplierdashboard');
       }else if(Auth::user()->roles[0]->name=='SLA'){
             return redirect('/admin/supplierdashboard');
       }else if(Auth::user()->roles[0]->name=='CM'){
            return redirect('/admin/companydashboard');
       }
    }

    public function salesDashboard()
    {
        return view('admin.salesdashboard');  
    }
    public function adminDashboard()
    {
        return view('admin.dashboard');  
    }
    public function commercialDashboard()
    {
        return view('admin.commercialdashboard');  
    }
    public function backofficeDashboard()
    {
        return view('admin.backofficedashboard');  
    }
    public function supplierOpDashboard()
    {
        return view('admin.supplierdashboard');  
    }
    public function supplierADashboard()
    {
        return view('admin.supplierdashboard');  
    }
     public function companyDashboard()
    {
        return view('admin.companydashboard');  
    }

    public function getGiveRolePermissions()
    {
        $roles = Role::with('permissions')->get();
        $permissions = Permission::with('child')->parent()->get();
        return view('admin.permissions.role-give-permissions', compact('roles', 'permissions'));
    }

    public function postGiveRolePermissions(Request $request)
    {
        $this->validate($request, ['role' => 'required', 'permissions' => 'required']);
        $role = Role::with('permissions')->whereName($request->role)->first();
        $role->permissions()->detach();
        foreach ($request->permissions as $permission_name) {
            $permission = Permission::whereName($permission_name)->first();
            $role->givePermissionTo($permission);
        }
        Session::flash('flash_success', _('Permission granted!'));
        return redirect('admin/roles');
    }

    public function getLangTranslation($to,Request $request){
        if (!$request->has('filename')) {
            echo "not valid file "; exit;
        }
        \App::setLocale('en');
        $langs = \Lang::get($request->filename);
        print "--------------------------------".$request->filename."-------------------------------------------<br><br><br>";
        foreach ($langs as $key=>$val){
            if(is_array($val)){
                foreach ($val as $k=>$s){
                    if(is_array($s)){
                        foreach ($s as $d=>$j){
                            if(!is_array($j)) $langs[$key][$k][$d] = $this->doTranslator('en',$to,$j);
                        }

                    }else{
                        $langs[$key][$k] = $this->doTranslator('en',$to,$s);
                    }

                }
            }else{
                $langs[$key] = $this->doTranslator('en',$to,$val);
            }
        }
        print "<pre>";
        var_export($langs);
        print "<br><br><br>---------------------------------------------------------------------------";
        exit;
    }

    public function doTranslator($from,$to,$data){
        $translator = new Translator;
        try{
            $data = $translator->setSourceLang($from)
                ->setTargetLang($to)
                ->translate($data);

        }catch (\Exception $e){
            //$desc =  $e->getMessage();
        }
        return html_entity_decode($data, ENT_QUOTES, "utf-8");
    }

    public function setInSession(Request $request){

        if(isset($request->menubar) && isset($request->click_count)){
			\Session::put('menubar',$request->menubar);
			\Session::put('click_count',$request->click_count);
        }
        return response()->json([
            'messages' => '',
        ], 200);
    }
}
