<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Supplier;
use App\Invoice;
use App\Transaction;
use App\TransactionsInvoices;
use DB;
use Yajra\Datatables\Datatables;
use App\User;
use Auth;
use App\SupplierUser;
use App\CustomerUser;


class SummaryController extends Controller
{   
    public function __construct()
    {
        $this->middleware('permission:access.receivable.summary');
    }


    public function index(Request $request){


        $startdate = date('01-m-Y');

        $enddate = date('t-m-Y');

    	return view('admin.summary.index',compact('startdate','enddate'));


    }

    public function datatable(Request $request){
		
		/*
		SELECT *,count(invoices.id) as total_invoice,sum(invoices.total_amount) as invoicesum 
		FROM supplier 
		JOIN invoices ON invoices.supplier_id = supplier.id
		JOIN transactions_invoices ON transactions_invoices.invoice_id = invoices.id
		WHERE transactions_invoices.payment_status != "cancelled"
		AND transactions_invoices.approver_id != 0
		GROUP BY transactions_invoices.supplier_id;
        */
        $user = User::find(Auth::user()->id);
		//dd($user->roles[0]->name);
        if($user->roles[0]->name == "SU"){
            $summary = Supplier::with('summary_invoice','summary_anticipation','summary_cession','cbuser','rate')
            ->Join('invoices','invoices.supplier_id','=','supplier.id')
            ->Join('transactions_invoices','transactions_invoices.invoice_id','=','invoices.id')
             ->where('transactions_invoices.payment_status','!=','cancelled')
             ->where('transactions_invoices.approver_id','!=',0)
             ->GROUPBY('transactions_invoices.supplier_id')
            ->select('supplier.*','supplier.name as supplier_name',DB::raw('avg(invoices.discount_rate) as mean_rate'));
        }
        elseif($user->roles[0]->name == "CBC"){

            $sup_user = SupplierUser::where('user_id',$user->id)->pluck('supplier_id');

            $summary = Supplier::with('summary_invoice','summary_anticipation','summary_cession','cbuser','rate')
            ->join('invoices','invoices.supplier_id','=','supplier.id')
            ->join('transactions_invoices','transactions_invoices.invoice_id','=','invoices.id')
            ->where('transactions_invoices.payment_status','!=','cancelled')
            ->where('transactions_invoices.approver_id','!=',0)
            ->GROUPBY('transactions_invoices.supplier_id')
            ->select('supplier.*','supplier.name as supplier_name',DB::raw('avg(invoices.discount_rate) as mean_rate'));

        }elseif($user->roles[0]->name == "SLA" || $user->roles[0]->name == "SLO"){

            $sup_user = SupplierUser::where('user_id',$user->id)->pluck('supplier_id');

            $summary = Supplier::with('summary_invoice','summary_anticipation','summary_cession','cbuser','rate')
            ->join('invoices','invoices.supplier_id','=','supplier.id')
            ->join('transactions_invoices','transactions_invoices.invoice_id','=','invoices.id')
            ->where('transactions_invoices.payment_status','!=','cancelled')
            ->where('transactions_invoices.approver_id','!=',0)
            ->GROUPBY('transactions_invoices.supplier_id')
            ->select('supplier.*','supplier.name as supplier_name',DB::raw('avg(invoices.discount_rate) as mean_rate'));

        }else{
			$summary = Supplier::with('summary_invoice','summary_anticipation','summary_cession','cbuser','rate')
            ->join('invoices','invoices.supplier_id','=','supplier.id')
            ->join('transactions_invoices','transactions_invoices.invoice_id','=','invoices.id')
            ->where('transactions_invoices.payment_status','!=','cancelled')
            ->where('transactions_invoices.approver_id','!=',0)
            ->GROUPBY('transactions_invoices.supplier_id')
            ->select('supplier.*','supplier.name as supplier_name',DB::raw('avg(invoices.discount_rate) as mean_rate'))->where('supplier.id',0);
		}
        
        //dd( $summary->get()); 
                        
        if ($request->has('startdate') && $request->get('startdate') != '') {
            
            $sdate=$request->get('startdate');
            $edate=$request->get('enddate');
            $stdate = str_replace('/','-',$sdate);
            $startdate= date('Y-m-d' , strtotime($stdate));
            $endate = str_replace('/','-',$edate);
            $enddate= date('Y-m-d' , strtotime($endate));
            $summary->whereBetween('transactions_invoices.created_at',[$startdate,$enddate]);
        }
        
        if ($request->has('supp_name') && $request->get('supp_name') != '') {
            
            $value = $request->supp_name;
            $summary->where('supplier.name','LIKE',"%$value%");
        }

        //dd($summary->get());
		
        $summary = collect($summary->get());       
        return Datatables::of($summary)->make(true);

    }
}