<?php

namespace App\Http\Controllers\Admin;

use App\Notifications\AddUser;
use App\Notifications\UserStatus;
use App\Notifications\CreateuserEmail;

use App\Http\Controllers\Controller;
use App\Role;
use App\User;
use App\Supplier;
use App\Customer;
use App\SupplierUser;
use App\CustomerUser;
use Yajra\Datatables\Datatables;
use DB;
use Illuminate\Http\Request;
use Session;
use App\ActionLog;
use App\CbuserChangeGoal;

class UsersController extends Controller

{

    public function __construct()
    {
        $this->middleware('permission:access.users');
        $this->middleware('permission:access.user.edit')->only(['edit', 'update']);
        $this->middleware('permission:access.user.create')->only(['create', 'store']);
        $this->middleware('permission:access.user.delete')->only('destroy');
    }


    public function index(Request $request)
    {
        $customer_id = "";
        $supplier_id = "";
        if($request->has('customer_id') && $request->get('customer_id') != "" ){

            $customer_id = $request->get('customer_id');
            $customer = Customer::find($customer_id);
        }
        if($request->has('supplier_id') && $request->get('supplier_id') != "" ){

            $supplier_id = $request->get('supplier_id');
            $supplier = Supplier::find($supplier_id);
        }

        $role = Role::pluck('label', 'id')->prepend('Filter Users by Roles', '');
        return view('admin.users.index', compact('role','customer_id','supplier_id','customer','supplier'));
    }

    public function datatable(request $request)
    {
        $user = User::select('users.*','customer.name as customer_name','roles.name as role_name','roles.id as role_id','roles.label as role_label','supplier.name as supplier_name')
            ->join('role_user', 'users.id', '=', 'role_user.user_id')
            ->join('roles', 'roles.id', '=', 'role_user.role_id')
            ->leftJoin('customer_user', 'customer_user.user_id', '=', 'users.id')
            ->leftJoin('customer', 'customer.id', '=', 'customer_user.customer_id')
            ->leftJoin('supplier_user', 'supplier_user.user_id', '=', 'users.id')
            ->leftJoin('supplier', 'supplier.id', '=', 'supplier_user.supplier_id');


        if ($request->has('filter_role') && $request->get('filter_role') != '') {
            $user->where('roles.id', $request->get('filter_role'),'OR');
        }
        if ($request->has('filter_status') && $request->get('filter_status') != '' && $request->get('filter_status') != 'all') {
            $user->where('users.user_status', $request->get('filter_status'), 'OR');
        }
        if ($request->has('order')) {
            $order = $request->get('order');
            if($order[0]['column'] == 4 && $order[0]['dir'] != ''){
                $user->orderBy('supplier.name', $order[0]['dir']);
            }
        }

        if($request->has('search') && $request->get('search') != '' ){
            $search = $request->get('search');
            if($search['value'] != ''){
                $value = $search['value'];
                $where_filter = "(customer.name LIKE  '%$value%' OR supplier.name LIKE  '%$value%' OR users.name LIKE  '%$value%' OR users.email LIKE  '%$value%'  )";
                $user->whereRaw($where_filter);
            }
        }

        if($request->has('customer_id') && $request->get('customer_id') != ''){
            $cm = 'CM';

           $user->where('roles.name',$cm)->where('customer_user.customer_id',$request->get('customer_id'));
           

        }
        if($request->has('supplier_id') && $request->get('supplier_id') != ''){

            $user->whereIn('roles.name', array('SLO', 'SLA'))->where('supplier_user.supplier_id',$request->get('supplier_id'));
        
        }


        $user->groupBy('users.id');

        return Datatables::of($user)
            ->make(true);
        exit;
    }

    public function create(Request $request)
    {  
        
        $roles = Role::select('id', 'name', 'label')->lower()->get();
        $roles = $roles->pluck('label', 'name');

        $role = $request->role;
        if(!empty($role)){
            if($role == 'SLO' || $role == 'SLA'){
                $supplier = Supplier::pluck('name','id');
                return response()->json($supplier);
            }elseif($role == 'CM'){
                $customer = Customer::pluck('name','id');
                return response()->json($customer);
            }
            elseif($role == 'SU' || $role == 'CBA' || $role == 'CBB' || $role == 'CBS' || $role == 'CBC'){
                $related = array('ConnectBahn');
                return response()->json($related);
            }
        }
        $customer_id = "";
        $supplier_id = "";
        $user_roles = '';
        if(!empty($request->get('customer_id'))){
            $customer_id  = $request->get('customer_id');
            $user_roles = 'CM';

        }
        if (!empty($request->get('supplier_id'))) {
            $supplier_id = $request->get('supplier_id');
            $user_roles = 'SLO';

        }

        return view('admin.users.create', compact('roles','customer_id','user_roles','supplier_id'));
    }

    public function store(Request $request)
    {
        

        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|unique:users',
            'roles' => 'required',
            'related' => 'required',
            'cpnj' => 'required',
            'phone' => 'required',
            'user_status' => 'required',
            'mobile' => 'required'
        ]);
		

        $data = $request->except('password');
        $data['password'] = "";
		

        if ($request->file('image')) {
            
                        $image = $request->file('image');
                        $filename = uniqid(time()) . '.' . $image->getClientOriginalExtension();
            
                        $image->move(public_path('images'), $filename);
            
                        $data['image'] = $filename;
            
                    }

		$data['remember_token'] = $request->_token;
        $user = User::create($data);

        $user->save();
        if ($user) {
           
            $user->notify(new CreateuserEmail($user));
            if($request->roles == 'SLA' or $request->roles == 'SLO'){
            
                $supplier_user = new SupplierUser;
                $supplier_user->user_id = $user->id;
                $supplier_user->supplier_id = $request->related;
                $supplier_user->save();

                \ActionLog::addToLog("Add SupplierUser","New relation SupplierUser is added, supplier_id = ".$supplier_user->supplier_id." and user_id = ".$supplier_user->user_id."",$supplier_user->getTAble(),$supplier_user->id);
                
            }
        if($request->roles == 'CM'  && !empty($request->related) ){
            
                $customer_user = new CustomerUser;
                $customer_user->user_id = $user->id;
                $customer_user->customer_id = $request->related;
                $customer_user->save();

                \ActionLog::addToLog("Add CustomerUser","New relation CustomerUser is added, customer_id = ".$customer_user->customer_id." and user_id = ".$customer_user->user_id."",$customer_user->getTAble(),$customer_user->id);

            } 
        }

        
        $user->assignRole($request->roles);
        

        Session::flash('flash_success', __('User added!'));

        \ActionLog::addToLog("Add User"," New User ". $user->name ." is added ",$user->getTAble(),$user->id);

        // send notification to admin & backoffice when user is added
        

        if(\Auth::check()){

            if(\Auth::user()->roles[0]->name == "SU"){
                //if user is added by admin, notification send to all backoffice

                $cbb = Role::Where('name','CBB')->get();

                if($cbb != ""){
                    foreach($cbb as $roles){
                        foreach($roles->users as $cbbuser){
                            $user_id[] = $cbbuser->id ;
                        } 
                    }
                }
                $users = User::whereIN('id',$user_id)->get();


                foreach($users as $bkouser){

                    $bkouser->notify(new AddUser($user));
                }
            }else{

                //if user is added by any backoffice then notification send to admin
                $su = Role::Where('name','SU')->get();

                if($su != ""){
                    foreach($su as $roles){
                        foreach($roles->users as $su_user){
                            $user_id[] = $su_user->id ;
                        } 
                    }
                }
                $users = User::whereIN('id',$user_id)->get();

                foreach($users as $adminuser){

                    $adminuser->notify(new AddUser($user));
                }
            }
        }

        return redirect('admin/users');
    }

    public function show(Request $request,$id)
    {   

        //notification
        if(\Auth::check()){

            if(\Auth::user()->roles[0]->name == "SU"){
                //if user is added by admin, notification send to all backoffice

                $cbb = Role::Where('name','CBB')->get();

                if($cbb != ""){
                    foreach($cbb as $roles){
                        foreach($roles->users as $cbbuser){
                            $user_id[] = $cbbuser->id ;
                        } 
                    }
                }
                $users = User::whereIN('id',$user_id)->get();

            }else{

                //if user is added by any backoffice then notification send to admin
                $su = Role::Where('name','SU')->get();

                if($su != ""){
                    foreach($su as $roles){
                        foreach($roles->users as $su_user){
                            $user_id[] = $su_user->id ;
                        } 
                    }
                }
                $users = User::whereIN('id',$user_id)->get();

            }
        }

        $user = User::findOrFail($id);
        
        //change users status
        $status = $request->get('status');
        if(!empty($status)){
            if($status == 'active' ){
                $user->user_status= 'inactive';
                $user->update();

                \ActionLog::addToLog("Inactive User"," User status changed from ". $status ." to ".$user->user_status."",$user->getTAble(),$user->id);

                foreach($users as $notifyuser){

                    $notifyuser->notify(new UserStatus($user));
                }

                return redirect()->back();
            }elseif($status ==  'inactive'){
                $user->user_status= 'active';
                $user->update();
                
                \ActionLog::addToLog("Active User"," User status changed from ". $status ." to ".$user->user_status."",$user->getTAble(),$user->id);

                foreach($users as $notifyuser){

                    $notifyuser->notify(new UserStatus($user));
                }
                
                return redirect()->back();
            }else{
                $user->user_status= 'active';
                $user->update();
                
                \ActionLog::addToLog(" Unblock User"," User status changed from ". $status ." to ".$user->user_status."",$user->getTAble(),$user->id);

                foreach($users as $notifyuser){

                    $notifyuser->notify(new UserStatus($user));
                }
                
                return redirect()->back();
            }

        }

        return view('admin.users.show', compact('user'));
    }

    public function edit(Request $request,$id)
    {
        $roles = Role::select('id', 'name', 'label')->lower()->get();
        $roles = $roles->pluck('label', 'name');

        $user = User::with('roles','customer','supplier')->findOrFail($id);
        
        $user_roles = [];
        $user_customer = [];
        $user_supplier = [];            

        foreach ($user->roles as $role) {
            $user_roles = $role->name;
        }


        $supplier = Supplier::all();
        $usersup = array();
        foreach ($supplier as $sup) {
                $usersup[$sup->id] = $sup->name;
        }
        $user_supplier = $usersup;



        $customer = Customer::all();
        $usercust = array();
        foreach ($customer as $cust) {

            $usercust[$cust->id] = $cust->name;
                
        }
        $user_customer = $usercust;

        $related = array('ConnectBahn');

        $role = $request->role;
        if(!empty($role)){
            if($role == 'SLO' || $role == 'SLA'){
                
                return response()->json($user_supplier);
            }elseif($role == 'CM'){
                
                return response()->json($user_customer);
            }
            elseif($role == 'SU' || $role == 'CBA' || $role == 'CBB' || $role == 'CBS' || $role == 'CBC'){

                return response()->json($related);
            }
        }


        
        
        return view('admin.users.edit', compact('user', 'roles', 'user_roles','user_customer','user_supplier','related'));

    }

    public function update($id, Request $request)
    {
        $this->validate($request,[
            
            'name' => 'required',
            'email' => 'required|unique:users,email,' . $id,
           // 'password' => 'regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{8,}/',
            'roles' => 'required',
            'related' => 'required',
            'cpnj' => 'required',
            'phone' => 'required',
            'user_status' => 'required',
            'mobile' => 'required'
        ]);

        $data = $request->except('password');
        if ($request->has('password')) {
            $data['password'] = bcrypt($request->password);
        }
        $user = User::findOrFail($id);

        if ($request->file('image')) {

            
            $image = $request->file('image');

            $filename = uniqid(time()) . '.' . $image->getClientOriginalExtension();
            
            $image->move(public_path('images'), $filename);
            
            $user->image = $filename;
            
        }

        $user->update($data);
        \ActionLog::addToLog("Edit User","User's data is updated , Name:".$user->name."" ,$user->getTAble(),$user->id);
        if($request->roles == 'SLA' or $request->roles == 'SLO'){
            $supplier_user = SupplierUser::where('user_id', $user->id)->first();
            if(count($supplier_user) == 0){
                $supplier_user = new SupplierUser;
                $supplier_user->user_id = $user->id;
                $supplier_user->supplier_id = $request->related;
                $supplier_user->save();

                \ActionLog::addToLog("Add SupplierUser","New relation SupplierUser is added, supplier_id = ".$supplier_user->supplier_id." and user_id = ".$supplier_user->user_id."",$supplier_user->getTAble(),$supplier_user->id);
                
            }else{
               $supplier_user->supplier_id = $request->related;
                $supplier_user->save(); 

                \ActionLog::addToLog("Edit SupplierUser","SupplierUser relation is updated, Supplier_id changed to ". $supplier_user->supplier_id ." ",$supplier_user->getTAble(),$supplier_user->id);
            }
        }
        if($request->roles == 'CM'  && !empty($request->related) ){
            $customer_user = CustomerUser::where('user_id', $user->id)->first();
            if(count($customer_user) == 0){
                $customer_user = new CustomerUser;
                $customer_user->user_id = $user->id;
                $customer_user->customer_id = $request->related;
                $customer_user->save();

                \ActionLog::addToLog("Add CustomerUser","New relation CustomerUser is added, customer_id = ".$customer_user->customer_id." and user_id = ".$customer_user->user_id."",$customer_user->getTAble(),$customer_user->id);

            }else{
                $customer_user->customer_id = $request->related;
                $customer_user->save();

                \ActionLog::addToLog("Edit CustomerUser","CustomerUser relation is updated, customer_id changed to ". $customer_user->customer_id ." ",$customer_user->getTAble(),$customer_user->id);
            }

        }  
        if ($user) {
            $user->save();
        }
        $user->roles()->detach();
        $user->assignRole($request->roles);
       //     \ActionLog::addToLog("Role Assigned","User- " .$user->name . " assigned with role". $request->roles ." ",$user->getTAble(),$user->id);
        Session::flash('flash_success', __('User updated!'));
        return redirect('admin/users');
    }

    public function destroy($id)
    {
        $user = User::find($id);

        if($user->image != ""){
            $file = public_path('images/' .$user->image);
            if (file_exists($file)) {
                unlink($file);
            }
        }
        

        \ActionLog::addToLog("User Deleted"," User-" .$user->name . " is deleted",$user->getTAble(),$user->id);

        User::destroy($id);

        Session::flash('flash_success', __('User deleted!'));

        return redirect('admin/users');
    }

    public function cbUsers(){
        return view('admin.users.cbusersindex');
    }

    public function cbdatatable(request $request)
    {
        $user = User::select('users.*', 'roles.name as role_name', 'roles.id as role_id', 'roles.label as role_label','goal')
            ->with('supplierCount','customerCount')
            ->join('role_user', 'users.id', '=', 'role_user.user_id')
            ->join('roles', 'roles.id', '=', 'role_user.role_id')
            ->leftJoin('cbuser_goal', 'cbuser_goal.user_id','=','users.id')
            ->whereIn('roles.name',['CBS','CBC']);
        return Datatables::of($user)
            ->make(true);
        exit;
    }

    public function cbUsersAddGoal(request $request){
        if($request->input('submit')){
            $CbuserGoal = CbuserChangeGoal::where('user_id',$request->input('cbuser_id'))->get();
            if(count ($CbuserGoal) > 0){
                $CbuserGoalStore = CbuserChangeGoal::find($CbuserGoal[0]->id);
                $CbuserGoalStore->goal = $request->input('goalAmount');
                $CbuserGoalStore->save();
                \ActionLog::addToLog("Change ConnectBahn Commercial Goal","Goal Change", $CbuserGoalStore->getTAble(), $CbuserGoal[0]->id, array( 'changed_by' => Auth::user()->name));
                return json_encode(array('msg' => 'Success'));
            }else{
                $CbuserGoalStore = new CbuserChangeGoal;
                $CbuserGoalStore->user_id = $request->input('cbuser_id');
                $CbuserGoalStore->goal = $request->input('goalAmount');
                $CbuserGoalStore->save();
                \ActionLog::addToLog("Change ConnectBahn Commercial Goal", "Goal Change", $CbuserGoalStore->getTAble(), $CbuserGoal[0]->id, array('changed_by' => Auth::user()->name));
                return json_encode(array('msg' => 'Success'));
            }
            return json_encode(array('msg' => 'Error'));
        }else{
            return json_encode(array('msg' => 'Error'));
        }
        return json_encode(array('msg' => 'Error'));
    }
}
