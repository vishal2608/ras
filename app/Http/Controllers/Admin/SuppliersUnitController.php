<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Session;
use Yajra\Datatables\Datatables;
use App\Supplier;
use App\SupplierUnit;
use CountryState;
use DB;

class SuppliersUnitController extends Controller
{   
    public function __construct()
    {
        $this->middleware('permission:access.supplier.unit');
        $this->middleware('permission:edit.supplier.unit')->only(['edit', 'update']);
        $this->middleware('permission:add.supplier.unit')->only(['create', 'store']);
        $this->middleware('permission:delete.supplier.unit')->only('destroy');
        $this->middleware('permission:change.status.supplier.unit')->only('show');
        
    }


    public function index(Request $request)
    {   
        exit;
    }

    public function listSupplier(Request $request ,$id){
        $status = '';
        if(!empty($request->status)){
            $status = $request->get('status');
        }
        $supplier = $id;
        $supplier_data = Supplier::find($id);
        return view('admin.supplier_unit.index', compact( 'supplier','supplier_data','status'));
    }

    public function datatable(Request $request , $id){
        
        $supplierunit = SupplierUnit::with('city','state')->where('supplier_id',$id);
        if ($request->has('status') && $request->status != ''){
            $supplierunit->where('status','active');
        }
        return Datatables::of($supplierunit)
            ->make(true);
            exit;
    }

    public function create(Request $request)
    {   
        $countries = DB::table("countries")->pluck('name','id')->prepend('Select Country');

        $supplier = Supplier::pluck('name','id');

        $supplier_id = '';
        if(!empty($request->get('supplier_id'))){
            $supplier_id =  $request->get('supplier_id');
            $supplier = Supplier::where('id',$supplier_id)->pluck('name','id');
        }

        return view('admin.supplier_unit.create',compact('countries','supplier','supplier_id'));
    }


    public function store(Request $request)
    {
        $messages = [
            'unit_name.unique' => "There's already a Supplier Unit with same Unit Name",
             'cpnj.unique' => "Supplier Unit Already exist, $request->cpnj using this CNPJ",
            
         ];
         $this->validate($request, [
            'unit_name' => 'required|unique:supplier_unit,unit_name',  
           'cpnj' => 'required|unique:supplier_unit,cpnj',
          

         ],$messages);
        
        $requestData = $request->all();

        $requestData['status'] = 'active';
        
        $supunit = SupplierUnit::create($requestData);

        \ActionLog::addToLog("Add Supplier-Unit","Supplier Unit- ".$supunit->unit_name." is added.",$supunit->getTAble(),$supunit->id);

        Session::flash('flash_message', 'SupplierUnit added!');

        return redirect('admin/suppliers');
    }

    public function show(Request $request,$id)
    {
        $supplierunit = SupplierUnit::findOrFail($id);

        //change users status
        $status = $request->get('status');
        if(!empty($status)){
            if($status == 'active' ){
                $supplierunit->status= 'inactive';
                $supplierunit->update();

                \ActionLog::addToLog("Supplier Unit Status Changed","Supplier Unit - ". $supplierunit->name ."'s status changed to Inactive",$supplierunit->getTAble(),$supplierunit->id);

                return redirect()->back();

            }else{

                $supplierunit->status= 'active';
                $supplierunit->update();
                
                \ActionLog::addToLog("Supplier Unit Status Changed","Supplier Unit - ". $supplierunit->name ."'s status changed to Active",$supplierunit->getTAble(),$supplierunit->id);

                return redirect()->back();
            }
        }

        return view('admin.supplier_unit.show', compact('supplierunit'));
    }

    public function editSupplierUnits($supplierid,$supplier_unit){

        $supplierunit = SupplierUnit::findOrFail($supplier_unit);    
        $supplier = Supplier::pluck('name', 'id');
        
        $countries = DB::table("countries")->pluck('name','id')->prepend('Select Country');
        $country_id=$supplierunit->country;
        $city_id=$supplierunit->city;        
        $state_id=$supplierunit->state;
        $states = DB::table("states")
                    ->where("country_id",$country_id)
                    ->pluck("name","id");
                   
        $cities = DB::table("cities")
                    ->where("state_id",$state_id)
                    ->pluck("name","id");
                    
        return view('admin.supplier_unit.edit', compact('state_id','states','country_id','supplierunit', 'countries', 'supplier','cities','city_id'));


    }

    public function edit($id)
    {
        $supplierunit = SupplierUnit::findOrFail($id);
        $supplier = Supplier::pluck('name','id');
        $countries = CountryState::getCountries();
        
        $states = CountryState::getStates('BR');

        return view('admin.supplier_unit.edit', compact('supplierunit','countries','states','supplier'));
    }

    public function update($id, Request $request)
    {   
        $messages = [
            'unit_name.unique' => "There's already a Supplier Unit with same Unit Name",
             'cpnj.unique' =>"Supplier Unit Already exist, $request->cpnj using this CNPJ",
            
         ];

         $this->validate($request, [
            'unit_name' => 'required|unique:supplier_unit,unit_name,'.$id,
            'cpnj' => 'required|unique:supplier_unit,cpnj,'.$id,
           

         ],$messages);
        
        $requestData = $request->all();
        
        $supplierunit = SupplierUnit::findOrFail($id);
        $supplierunit->update($requestData);

        \ActionLog::addToLog("Edit Supplier-Unit","Supplier Unit- ".$supplierunit->unit_name." is updated.",$supplierunit->getTAble(),$supplierunit->id);

        Session::flash('flash_message', 'SupplierUnit updated!');

        return redirect('admin/supplier_unit/list/'.$request->supplier_id);
    }

    public function destroy($id)
    {
        $supplierunit = SupplierUnit::find($id);

        \ActionLog::addToLog("Delete Supplier-Unit","Supplier Unit- ".$supplierunit->unit_name." is deleted.",$supplierunit->getTAble(),$supplierunit->id);

        $supplierunit->delete();

        Session::flash('flash_message', 'SupplierUnit deleted!');

    }

}
