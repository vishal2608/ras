<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//
//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});


// Route::group(['prefix' => 'v1.0/ticket', 'middleware' => 'api_pass', 'namespace' => 'Api'], function () {
//     //
//     Route::get('/get-subjects', 'TicketController@getSubjects');
//     Route::get('/get-subject/{subject_id}', 'TicketController@getSubject');
//     //
//     Route::get('/get-tickets', 'TicketController@getTickets');
//     Route::get('/get-ticket/{ticket_id}', 'TicketController@getTicket');
// //    //
//     Route::post('/create-ticket', 'TicketController@postTicket');
//     Route::put('/edit-ticket/{ticket_id}', 'TicketController@putTicket');
//     //
//     Route::delete('/delete-ticket/{ticket_id}', 'TicketController@deleteTicket');


// });

