<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'status_dropdown' => [
        'new' => 'New',
        'open' => 'Open',
        'pending' => 'Pending',
        'on-hold' => 'On-hold',
        'solved' => 'Solved',
        'closed' => 'Closed'
    ],
    'label'=>[
        'dashboard'=>'Dashboard',
        'all_tickets'=>'All Tickets',
        'sales_dashboard' => 'Sales Dashboard',
        'back_dashboard' => 'BackOffice Dashboard',
        'comm_dashboard' => 'Commercial Dashboard',
        'supplier_dashboard'=> 'Suppliers Dashboard',
        'company_dashboard'=> 'Company Dashboard',
        'transactions'=>'Transactions',
        'receivable'=>'Receivable',
        'summary'=>'summary',
        'invoices'=>'Invoices',
        'users'=>'Users',
        'users_log'=>'Users Logs',
        'manage'=>'Manage',
        'banks'=>'Banks',
        'customers'=>'Customers',
        'supplier'=>'Supplier',
        'errormessage'=>"Oops! You Don't Have Permission !!",
    ],
];
