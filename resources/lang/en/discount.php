<?php

return[ 

	'discount_rate' => 'Discount Rate',
	'add_new' => 'Add New',
	'id' => 'ID',
	'class' => 'Class',
	'minimum' => 'Minimum',
	'maximum' => 'Maximum',
	'actions' => 'Actions',
	'view' => 'View',
	'edit' => 'Edit',
	'back' => 'Back',
	'edit_discount_rate' => 'Edit Discount rate',
	'create_new_discount_rate' => 'Craete New Discount rate',
	'update' => 'Update',
	'create' => 'Create',

];