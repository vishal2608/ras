<?php

return[

	'view' =>'View',
	'edit' => 'Edit',
	'delete' => 'Delete',
	'back' =>'Back',
	'action_log' => 'Action Log',
	'actioner_id' => 'Actioner ID',
	'actioner_name' => 'Actioner Name',
	'actioner_role' => 'Actioner Role',
	'action' => 'Action',
	'detail' => 'Detail',
	'role' => 'Role',
	'table_name' => 'Table Name',
	'field_id' => 'Field ID',
	'url' => 'URL',
	'ip' => 'IP',
	'id' => 'ID',
	'date' => 'Date',
	'company_name' => 'Company Name',
	'view_log' => 'View Log',
	'period' => 'Period',
	'from' => 'From',
	'to' => 'To',
	'show' => 'Show',
	


];