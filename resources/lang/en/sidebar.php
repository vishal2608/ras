<?php

return [

	'customer' => 'Customer',
	'my_profile' => 'My Profile',
	'suppliers' => 'Suppliers',
	'banks' => 'Banks',
	'transaction' => 'Transaction',
	'discount_rate' => 'Discount Rate',
	'invoice' => 'Invoices',
	'dashboard' => 'Dashboard',

];