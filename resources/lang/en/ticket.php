<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'label'=>[
        'mark_ticket_as'=>'Mark ticket as ...',
        'mark_as'=>'Mark as',
        'evidence'=>'Evidence',
        'evidence'=>'Evidence',
        'note'=>'Note',
        'place_a_comment'=>'Place a Comment',
        'id'=>'Id',
        'name'=>'Name',
        'title'=>'Title',
        'subject'=>'Subject',
        'content'=>'Content',
        'status'=>'Status',
        'site'=>'Site',
        'equipment_id'=>'Equipment Id',
        'website'=>'Website',
        'create_ticket'=>'Create Ticket',
        'edit_ticket'=>'Edit Ticket',
        'added_a_file'=>'Added a File',
        'show_ticket'=>'Show Ticket',
        'add_your_comment_placeorder'=>'Type your comment here....',
    ],
];
