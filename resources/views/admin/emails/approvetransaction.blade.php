<!DOCTYPE html>
<html>
	<head>
		<title>ConnectBahn</title>
		<style>
			h2 {
				color:#BF1E2E;
				font-size: 20px;
				text-align: center;
			}
			table {
				border-collapse: collapse;
				width: 50%;
				text-align: center;
				margin:0px auto 20px auto;
        
			}

			th, td {
				text-align: left;
				padding: 8px;
				width: 50%;
				font-size: 15px;
			}

			tr:nth-child(odd){background-color: #f2f2f2}

			th {
				background-color: #4CAF50;
				color: white;
			}
			
			p {
				margin:30px; 
				font-size:15px;
			}
		
		</style>
	</head>
	<body style="margin:30px auto; width:700px;">
		
		<div style="align:center; border:1px solid #ccc; padding-top:30px; padding-bottom:40px">
			<h2>Transaction Approval</h2>
			<p>Hi,</p>	
			<table>	
		
	
		<?php 
		/*foreach($customeruser as $customer)	{
		   foreach($customer->getCustomerApproveTransactionRoles as $customermail){
			
			}
			foreach($customer->getSupplierApproveTransactionRoles as $suppliermail){
			}
			
	   
	   } */
	   //echo '<pre>';print_r($trans);echo '</pre>';
	   //echo '<pre>';print_r($customeruser);echo '</pre>';
	   

		?>
				<tr>
					<td>Transaction #{{$trans->id}} has been approved. Find details below:</td>					
				</tr>
				<tr>
					<td>Approval date: {{$trans->updated_at->todatestring()}}</td>					
				</tr>
				<tr>
					<td>Customer: {{$customeruser[0]->getCustomerApproveTransactionRoles[0]->customer_name}}</td>					
				</tr>
				<tr>
					<td>Supplier: {{$customeruser[0]->getSupplierApproveTransactionRoles[0]->supplier_name}}</td>					
				</tr>
				<tr>
					<td>Total amount: {{$trans->total_amount}}</td>					
				</tr>
				<tr>
					<td>Rate: {{$customeruser[0]->discount_rate}}</td>					
				</tr>				
				<tr>
					<td>Type: {{$trans->type}} with {{$customeruser[0]->banks_name}}</td>					
				</tr>
				<tr>
					<td>Net value: {{$trans->net_value}}</td>					
				</tr>

				<tr>
					<td>For more details, access this <a href="{{url('/admin/transactions/'.$trans->id)}}">link</a>.</td>					
				</tr>
				
			</table>
			<p>ConnectBahn Team</p>
		</div>
		
	</body>
</html>