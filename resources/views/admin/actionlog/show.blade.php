@extends('layouts.backend')

@section('title',trans('actionlog.action_log'))


@section('content')
    <div class="row">

        <div class="col-md-12">
            <div class="box bordered-box blue-border">
                              <div class="box-header blue-background">
                                                  <div class="title">
                                                      <i class="icon-circle-blank"></i>
                                                     @lang('actionlog.action_log')
                                                  </div>

                               </div>
                               <div class="box-content ">
            

                    <a href="{{ URL::previous() }}" title="Back">
                        <button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> @lang('actionlog.back')
                        </button>
                    </a>
                    
                    {!! Form::open([
                        'method' => 'DELETE',
                        'url' => ['/admin/action-log', $actionlog->id],
                        'style' => 'display:inline'
                    ]) !!}
                    {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                            'type' => 'submit',
                            'class' => 'btn btn-danger btn-xs',
                            'title' => 'Delete User',
                            'onclick'=>'return confirm("Confirm delete?")'
                    ))!!}
                    {!! Form::close() !!}
                    <br/>
                    <br/>



                    <div class="table-responsive">
                        <table class="table table-borderless">
                            <tbody>

                            <tr>
                                <td>@lang('actionlog.id')</td>
                                <td>{{ $actionlog->id }}</td>
                            </tr>

                            <tr>
                                <td>@lang('actionlog.actioner_id')</td>
                                <td>{{ $actionlog->actioner_id }}</td>
                            </tr>

                            <tr>
                                <td>@lang('actionlog.actioner_name')</td>
                                <td>{{ $actionlog->actioner_name }}</td>
                            </tr>

                            <tr>
                                <td>@lang('actionlog.company_name')</td>
                                <td>@if($actionlog->company != "")
                                        @if($actionlog->company->role_name == 'CM')
                                            
                                                {{$actionlog->company->customer_name}}
                                            
                                        @endif
                                        @if($actionlog->company->role_name == 'SLO' || $actionlog->company->role_name == 'SLA')
                                           {{$actionlog->company->supplier_name}}
                                        @endif
                                        @if($actionlog->company->role_name == 'SU' || $actionlog->company->role_name == 'CBA' || $actionlog->company->role_name == 'CBS' || $actionlog->company->role_name == 'CBC' || $actionlog->company->role_name == 'CBB')
                                                 ConnectBahn
                                        @endif
									@endif
                                </td>
                            </tr>

                            <tr>
                                <td>@lang('actionlog.date')</td>
                                <td>{{ \Carbon\Carbon::parse($actionlog->created_at)->format('d/m/Y')}}</td>
                            </tr>

                            <tr>
                                <td>@lang('actionlog.actioner_role')</td>
                                <td>{{ $actionlog->role }}</td>
                            </tr>

                            <tr>
                                <td>@lang('actionlog.action')</td>
                                <td>{{ $actionlog->action }}</td>
                            </tr>

                            <tr>
                                <td>@lang('actionlog.detail')</td>
                                <td>{{ $actionlog->detail }}</td>
                            </tr>

                            <tr>
                                <td>@lang('actionlog.table_name')</td>
                                <td>{{ $actionlog->tablename }}</td>
                            </tr>

                            <tr>
                                <td>@lang('actionlog.field_id')</td>
                                <td>{{ $actionlog->field_id }}</td>
                            </tr>

                            <tr>
                                <td>@lang('actionlog.url')</td>
                                <td>{{ $actionlog->url }}</td>
                            </tr>

                            <tr>
                                <td>@lang('actionlog.ip')</td>
                                <td>{{ $actionlog->ip }}</td>
                            </tr>

                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection