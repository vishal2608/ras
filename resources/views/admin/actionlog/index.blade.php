@extends('layouts.backend')

@section('title',trans('actionlog.action_log'))
@section('pageTitle',trans('actionlog.action_log'))

@section('content')
        <div class="row">
            <div class="col-md-12">
              <div class="box bordered-box blue-border">
                              <div class="box-header blue-background">
                                                  <div class="title">
                                                      <i class="icon-circle-blank"></i>
                                                     @lang('actionlog.action_log')
                                                  </div>

                               </div>
                               <div class="box-content ">

                                <a href="{{ URL::previous() }}" title="Back">
                        <button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> @lang('actionlog.back')
                        </button>
                    </a>

                                <label>@lang('actionlog.period') :</label>
                                    <label>@lang('actionlog.from')</label>
                                    <input type="text" name="firstday" id="startdate" value="{{$startdate}}">
                                    <label>@lang('actionlog.to')</label>
                                    <input type="text" name="lastday" id="enddate" value="{{$enddate}}">
                                    <button class="btn btn-success btn-xs" name="submit" value="submit" id="changedate" >@lang('actionlog.show')</button>

                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table table-borderless" id="log-table">
                                <thead>
                                    <tr>
                                        <th data-priority="1">@lang('actionlog.id')</th>
                                        
                                        <th data-priority="2">@lang('actionlog.actioner_name')</th>
                                        <th data-priority="3">@lang('actionlog.company_name')</th>
                                        <th data-priority="4">@lang('actionlog.actioner_role')</th>
                                        <th data-priority="5">@lang('actionlog.date')</th>
                                        <th data-priority="6">@lang('actionlog.action')</th>
                                        <th data-priority="7">@lang('actionlog.detail')</th>
                                        <th data-priority="8">@lang('actionlog.view_log')</th>
                                    </tr>
                                </thead>
                                
                            </table>
                            
                        </div>

                    </div>
                </div>
            </div>
        </div>
@endsection

@push('script-head')
<script>
var url ="{{ url('/admin/action-log/') }}";
     
        
        datatable = $('#log-table').DataTable({
            processing: true,
            serverSide: true,
            "order": [[ 4, "desc" ]],
            ajax: 
            {
                url: '{!! route('ActionLogControllerLogsData') !!}', // json datasource
                type: "get", // method , by default get
                data: function (d) {
                    d.startdate = $('#startdate').val();
                    d.enddate = $('#enddate').val();
                    d.user_id = "{{$user_id}}";
                }
            },
             
                columns: [
                    { data: 'id', name: 'Id',"searchable": false },
                     
                    { data: 'actioner_name', name: 'actioner_name',"searchable": true },
                    {
                        "data" : null,
                        "orderable": false,
                        "searchable": false,
                        render: function (o){
							if(o.company != null){
									var role = o.company.role_name;
									if(role == 'CM'){
										return o.company.customer_name;
									}
									
									if(role == 'SLO' || role == 'SLA' )
										return o.company.supplier_name;
									if(role == 'SU' || role == 'CBA' || role == 'CBS' || role == 'CBC' || role == 'CBB')
										return 'ConnectBahn';
									if(role == '')
										return 'Not Assign';
							}else{
								return '-';
							}
                        }
                        
                    },
                    { data: 'role', name: 'role' },
                   // { data: 'created_at', name: 'created_at' },
                    { 
                        "data":null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                                var createddate=o.created_at;
                               if(createddate!=null){
                                    var newdate = createddate.replace('-', '/').replace('-', '/');                               
                                    var first_date = moment(newdate).format('DD/MM/YYYY');
                                    return first_date
                                }else{
                                    var newdate ='';
                                    return newdate
                                } 
								
                            
                        }
                    },
                    { data: 'action', name: 'action' },
                    { data: 'detail', name: 'detail' ,"searchable":false},

                    { 
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {

                                var v =  "<a href='"+url+"/"+o.id+"' data-id="+o.id+"><button class='btn btn-warning btn-xs'><i class='fa fa-eye' aria-hidden='true'></i></button></a>&nbsp;";
  
                            return v;
                        }
                    }
                ]
        });

$('#changedate').click(function(){
    datatable.draw();
});

$(function () {
        $('#startdate').datetimepicker({
            format: 'DD/MM/YYYY',
            
        });
        $('#enddate').datetimepicker({
            format: 'DD/MM/YYYY',
        });
        $("#startdate").on("dp.change", function (e) {
            format: 'DD/MM/YYYY',
            $('#enddate').data("DateTimePicker").minDate(e.date);
        });
        $("#enddate").on("dp.change", function (e) {
            format: 'DD/MM/YYYY',
            $('#startdate').data("DateTimePicker").maxDate(e.date);
        });
    })

    
    
</script>
@endpush

