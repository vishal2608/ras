@extends('layouts.backend')
@section('title',trans('invoice.create_new_invoices'))

@section('pageTitle')
    <i class="icon-tint"></i>
    <span>@lang('invoice.create_new_invoices')</span>
@endsection

@section('content')
        <div class="row">
            <div class="col-md-12">
                <div class="box bordered-box blue-border">
                              <div class="box-header blue-background">
                                                  <div class="title">
                                                      <i class="icon-circle-blank"></i>
                                                     @lang('invoice.create_new_invoices')
                                                  </div>

                               </div>
                               <div class="box-content ">
                
                        <a href="{{ URL::previous() }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i>@lang('invoice.back')</button></a>
                        <br />
                        <br />

                        @if(Auth::user()->can('access.invoices.create'))
                               <a href="{{public_path('uploads/'). 'book.csv'}}" title="Download Sample"><button class="btn btn-warning btn-xs"><i class="fa fa-download" aria-hidden="true"></i>@lang('invoice.samplecsv')</button></a>
                            {!! Form::open(['url' => '/admin/invoice', 'class' => 'form-horizontal','method'=>'POST','enctype' => 'multipart/data','files'=>'true']) !!}

                                <div class="form-group">
                                    {!! Form::label('file',trans('invoice.upload_file'), ['class' => 'col-md-4 control-label']) !!}
                                    <div class="col-md-6">
                                        {!! Form::file('file', null, ['class' => 'form-control', 'required' => 'required']) !!}
                                        
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-offset-4 col-md-4">
                                        {!! Form::submit('Upload', ['class' => 'btn btn-primary']) !!}
                                    </div>
                                </div>

                            {!! Form::close() !!}
                        @endif

                        
                    </div>
                </div>
            </div>
        </div>
@endsection