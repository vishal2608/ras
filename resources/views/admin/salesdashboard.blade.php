@extends('layouts.backend')


@section('title',trans('dashboard.label.sales_dashboard'))

@push('js')
<style>

</style>
@endpush

@section('content')
 @if(Auth::user()->can('access.sales.dashboard'))
    <div class="box bordered-box blue-border">
        <div class="box-header blue-background">
            <div class="title">
                <i class="icon-circle-blank"></i>
                @lang('dashboard.label.sales_dashboard')
            </div>

        </div>
        <div class="panel-body">


            <div class="row">
               <div class="row state-overview">
			<div class="col-lg-3 col-sm-6">
				<a href="{{url('/admin/customer')}}">
					<section class="panel dash_panel">
						<div class="value">
							<h1 class="dash_text"> @lang('dashboard.label.manage') <br> @lang('dashboard.label.customers')</h1>						
						</div>
					</section>
				</a>
			</div>
			
			<div class="col-lg-3 col-sm-6">
				<a href="{{url('/admin/transactions')}}">
					<section class="panel dash_panel dash_panel_trans">
						<div class="value">
							<h1 class="dash_text"> @lang('dashboard.label.transactions')</h1>							
						</div>
					</section>
				</a>
			</div>
			<div class="col-lg-3 col-sm-6">
				<a href="{{url('/admin/summary')}}">
					<section class="panel dash_panel ">
						<div class="value">
							<h1 class="dash_text"> @lang('dashboard.label.receivable') <br>  @lang('dashboard.label.summary')</h1>							
						</div>
					</section>
				</a>
			</div>
			<div class="col-lg-3 col-sm-6">
                <a href="{{url('/admin/invoice')}}">
                    <section class="panel dash_panel dash_panel_trans">
                        <div class="value">
							<h1 class="dash_text"> @lang('dashboard.label.invoices')</h1>							
						</div>
                    </section>
                </a>
            </div>			
			
		</div>
            </div>


            <div class="table-responsive">


            </div>

        </div>
    </div>
@else
	<div class="row">
	   <div class="col-md-12">
            <div class="box bordered-box blue-border">
                <div class="box-header blue-background">
					<div class="title">
						<i class="icon-circle"></i>
						@lang('dashboard.label.errormessage')
					</div>
				</div>
			</div>
		</div>
	</div>
               
@endif
@endsection



@push('js')
<script>

</script>


@endpush
