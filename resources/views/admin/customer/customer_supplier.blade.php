@extends('layouts.backend')

@section('title',trans('customer.related_suppliers'))
@section('pageTitle',trans('customer.related_suppliers'))

@section('content')
        <div class="row">
            <div class="col-md-12">
              <div class="box bordered-box blue-border">
                              <div class="box-header blue-background">
                                                  <div class="title">
                                                      <i class="icon-circle-blank"></i>
                                                     @lang('customer.list_of_suppliers_from_customer') -  {{ $customer->name }}
                                                  </div>

                               </div>
                               <div class="box-content ">
                        <a href="{{ URL::previous() }}" title="Back">
                        <button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> @lang('customer.back')
                        </button>
                        </a>
                        <a href="{{ url('/admin/customer') }}" title="Back">
                        <button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> @lang('customer.back_to_list')
                        </button>
                        </a>

                        @if(Auth::user()->can('add.supplier.for.customer'))
                        <form method="post" action="{{url('/admin/customer/'.$customer_id.'/customersupplier')}}">
                            {{csrf_field()}}
                        <div class="form-group{{ $errors->has('supplier') ? ' has-error' : ''}}">
                            {!! Form::label('supplier', trans('customer.add_suppliers'), ['class' => 'col-md-2 control-label']) !!}
                            <div class="col-md-4">
                            {!! Form::select('supplier', $suppliers, ['class' => 'form-control supplier_se_list selectTag','id'=>'suppliers']) !!}
                            </div>

                        </div>
                        <div class="form-group">
                            <div class="col-md-offset-0 col-md-4">
                                <button type="submit" class="btn btn-success btn-xs sup_btn">Add Supplier</button>
                            </div>
                        </div>

                        </form>
                        @endif

                        <div class="table-responsive">
                            <table class="table table-borderless" id="discount-table">
                                <thead>
                                    <tr>
                                        <th data-priority="1">@lang('customer.id')</th>
                                        
                                        <th data-priority="3">@lang('customer.supplier_group_name')</th>

                                        <th data-priority="4">@lang('customer.supplier_units')</th>

                                        <th data-priority="5">@lang('customer.customer_rate_class')</th>

                                        <th data-priority="6">@lang('customer.cb_rate_class')</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                
                                <tbody>
                                @foreach($custsup as $item)
                                    <tr>
                                        <td>{{ $item->id }}</td>
                                        
                                        <td>{{ $item->supplier->name }}</td>
                                        <td>
                                            {{-- @if($item->supplier->unit != null)
                                                {{$item->supplier->unit->cpnj}}
                                            @else
                                                {{$item->supplier->cpnj}}
                                            @endif --}}
                                            @if(Auth::user()->can('access.supplier.unit'))
                                                <a href="{{ url('/admin/supplier_unit/list/'.$item->supplier->id.'?status=active') }}">{{ count($item->supplier->units) }}</a>
                                            @else
                                                {{ count($item->supplier->units) }}
                                            @endif
                                        </td>
                                       
                                       <td>
                                           
                                            @if($item->customer_supplier_rate != '')
                                                {{$item->customer_discount_rate->class}}
                                                
                                                @if(Auth::user()->can('edit.customer.rate.class'))
                                                    <a href="#"  class="btn btn-success btn-xs open_model_for_add_class_customer" data-supplierName="{{$item->supplier->name}}" data-supplierId="{{$item->supplier->id}}" data-action="edit" data-class="{{$item->customer_discount_rate->id}}" data-customerSupplierId="{{$item->id}}" title="Edit Class"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                                    </a>
                                                @endif
                                            @else
                                                @if(Auth::user()->can('add.customer.rate.class'))
                                                    <a href="#"  class="btn btn-success btn-xs open_model_for_add_class_customer" data-supplierName="{{$item->supplier->name}}" data-action="add" data-supplierId="{{$item->supplier->id}}" data-customerSupplierId="{{$item->id}}" data-class="0" title="Add Class"><i class="fa fa-plus" aria-hidden="true"></i> Add Class
                                                    </a>
                                                @endif    
                                            @endif
                                            
                                        <td>
                                        @if($item->customer_supplier_rate_by_commercial_user != '')
                                            {{$item->discountRate->class}}

                                            @if(Auth::user()->can('edit.cb.rate.class'))
                                                <a href="#"  class="btn btn-success btn-xs open_model_for_add_class" data-supplierName="{{$item->supplier->name}}" data-supplierId="{{$item->supplier->id}}" data-action="edit" data-class="{{$item->discountRate->id}}" data-customerSupplierId="{{$item->id}}" title="Edit Class"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                                </a>
                                            @endif
                                        @else

                                            @if(Auth::user()->can('add.cb.rate.class'))
                                                <a href="#"  class="btn btn-success btn-xs open_model_for_add_class" data-supplierName="{{$item->supplier->name}}" data-action="add" data-supplierId="{{$item->supplier->id}}" data-customerSupplierId="{{$item->id}}" data-class="0" title="Add Class"><i class="fa fa-plus" aria-hidden="true"></i> Add Class
                                                </a>
                                            @endif
                                        @endif
                                        </td>
                                        <td>
                                            @if(Auth::user()->can('delete.relation'))
                                                {!! Form::open([
                                                    'method'=>'DELETE',
                                                    'url' => ['/admin/customer/deleteCustSup',$item->id],
                                                    'style' => 'display:inline'
                                                ]) !!}
                                                    {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                                            'type' => 'submit',
                                                            'class' => 'btn btn-danger btn-xs',
                                                            'title' => 'Delete DiscountRate',
                                                            'onclick'=>'return confirm("Confirm delete?")'
                                                    )) !!}
                                                {!! Form::close() !!}
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                                
                            </table>
                            
                            <div class="pagination-wrapper"> {!! $custsup->appends(['search' => Request::get('search')])->render() !!} </div>
                            
                        </div>
                        @include("admin.models.add_class")
                        @include("admin.models.add_customer_discount_class")
                    </div>
                </div>
            </div>
        </div>
@endsection
@push('script-head')
<script>

$(document).ready(function() {
    $('#select_supplier').select2();
});

$(document).ready(function(){
    $(document).on('click', '.open_model_for_add_class', function (e) {
        if($(this).attr('data-action') == 'edit'){
            $('#account_form_model_lable').html('Edit Class(Supplier to Customer)')
            $("select#SelectClass").val($(this).attr('data-class'));
            $("#customer_supplier_id").val($(this).attr('data-customerSupplierId'));
        }else{
            $("select#SelectClass").val($(this).attr('data-class'));
            $("#customer_supplier_id").val($(this).attr('data-customerSupplierId'));
            $('#account_form_model_lable').html('Add Class(Supplier to Customer)')
        }
        
        $("#supplier_id").val($(this).attr('data-supplierId'));
        $("#supplier_name").html($(this).attr('data-supplierName'));
        var formid = $("#class_add_form");
        $('#classAdd').modal('show');
    });
    $("#class_add_form").validate({
        rules: {
            SelectClass: {
                required: true,
            }
        },
        messages: {
            SelectClass: {
                required: "Please Select Class",
            }
        },
        submitHandler: function (form) {
            var url = "{{url('admin/discount-rate/suppliertocustomer')}}";
            var method = "post"
            $.ajax({
                type: method,
                url: url,
                data: $(form).serialize(),
                beforeSend: function () {
                },
                success: function (result)
                {
                    result = JSON.parse(result)
                    if(result.msg == 'Success')
                        toastr.success('Added Successfully',result.message)
                    else
                        toastr.error('Something Went Wrong.Please Try Again!!',result.message)
                    $('#classAdd').modal('hide');
                    setTimeout(function(){
                        location.reload();
                    },3000);
                },
                error: function (error) {
                    $('#classAdd').modal('hide');
                }
            }); 
            return false;
        }
    });
});

$(document).ready(function(){
    $(document).on('click', '.open_model_for_add_class_customer', function (e) {
        if($(this).attr('data-action') == 'edit'){
            $('#account_form_model_lable').html('Edit Class(Supplier to Customer)')
            $("select#SelectClass").val($(this).attr('data-class'));
            $("#discount_customer_supplier_id").val($(this).attr('data-customerSupplierId'));
        }else{
            $("select#SelectClass").val($(this).attr('data-class'));
            $("#discount_customer_supplier_id").val($(this).attr('data-customerSupplierId'));
            $('#account_form_model_lable').html('Add Class(Supplier to Customer)')
        }
        
         $("#discount_supplier_id").val($(this).attr('data-supplierId'));
       
        $("#supplier_name").html($(this).attr('data-supplierName'));
        var formid = $("#class_add_form_customer");
        $('#classAdd_customer').modal('show');
    });
    $("#class_add_form_customer").validate({
        rules: {
            SelectClass: {
                required: true,
            }
        },
        messages: {
            SelectClass: {
                required: "Please Select Class",
            }
        },
        submitHandler: function (form) {
          //  console.log(form);
            var url = "{{url('admin/discount-rate/add-class-customer')}}";
            var method = "post"
            $.ajax({
                type: method,
                url: url,
                data: $(form).serialize(),
                beforeSend: function () {
                },
                success: function (result)
                {
                    result = JSON.parse(result)
                    if(result.msg == 'Success')
                        toastr.success('Added Successfully',result.message)
                    else
                        toastr.error('Something Went Wrong.Please Try Again!!',result.message)
                    $('#classAdd_customer').modal('hide');
                    setTimeout(function(){
                        location.reload();
                    },3000);
                },
                error: function (error) {
                    $('#classAdd_customer').modal('hide');
                }
            }); 
            return false;
        }
    });
});
   
</script>
@endpush


