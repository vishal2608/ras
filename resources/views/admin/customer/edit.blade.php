@extends('layouts.backend')

@section('title',trans('customer.edit_customer'))
@section('pageTitle',trans('customer.edit_customer'))



@section('content')
    <div class="row">

        <div class="col-md-12">
            <div class="box bordered-box blue-border">
                              <div class="box-header blue-background">
                                                  <div class="title">
                                                      <i class="icon-circle-blank"></i>
                                                     @lang('customer.edit_customer')
                                                  </div>

                               </div>
                               <div class="box-content ">
            
                    <a href="{{ URL::previous() }}" title="Back">
                        <button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> @lang('customer.back')
                        </button>
                    </a>
                    <br/>
                    <br/>

                    @if ($errors->any())
                        <ul class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif

                    @include("admin.models.cash_add")

                    {!! Form::model($customer, [
                        'method' => 'PATCH',
                        'url' => ['/admin/customer', $customer->id],
                        'class' => 'form-horizontal',
                        'id'=> 'formCustomer',
                        'enctype'=>'multipart/form-data'
                    ]) !!}

                    @include ('admin.customer.form', ['submitButtonText' => trans('customer.update')])

                    {!! Form::close() !!}
                    
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script-head')
<script>
$(document).ready(function(){
    $( ".countryOnchange" ).change(function() {
        var countryID = $(this).val();    
        if(countryID){
            $.ajax({
                type:"GET",
                url:"{{url('admin/getstate')}}?country_id="+countryID,
                success:function(res){               
                    if(res){
                        console.log(res);
                        $(".state").empty();
                        $.each(res,function(key,value){
                            $(".state").append('<option value="'+key+'">'+value+'</option>');
                        });
                    }else{
                       $(".state").empty();
                    }
                }
            });
        }else{
            $(".state").empty();
        }      
    });
    $( '#creatSupplier' ).on( 'submit', function(e) {
        
        var url = $(this).attr('action');
        moredata = '1';
        $input = $('<input type="hidden" name="ajax"/>').val(moredata);
        $('#creatSupplier').append($input);
        //console.log($(this).serialize());
        $.ajax({
            type: "POST",
            url: url,
            data: $(this).serialize(),
            success: function( responses ) {
                var response = JSON.parse(responses);
                if(response.msg == 'Success'){
                    console.log('Supplier Add');
                    $("#supplierList").empty();
                    $.each(response.supplier,function(key,value){
                        $("#supplierList").append('<option value="'+key+'">'+value+'</option>');
                    });
                    var msg = '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>{{ Session::get("flash_success") }}</div>';
                    $('#supplieradd').html(msg);
                    $('#supplierAddModal').modal('hide');
                }else{
                    console.log(response);
                }
            },
            error:function (responses){
               var error = JSON.parse(responses.responseText);
               $.each(error,function(key,value){
                   $('#errorSupplier').append('<p class="help-block">'+ value +'</p>')
               });
            }
        });
        e.preventDefault();
    });

    $(document).on('click', '#open_model_for_add_cash', function (e) {
        var formid = $("#cash_add_form");
        $('#cashAdd').modal('show');
    });

    $('#cash_expiry').datetimepicker({
        format: 'DD-MM-YYYY',
    });

    $("#cash_add_form").validate({
    rules: {
        add_cash_input: {
            required: true,
        },
        cash_expiry: {
            required: true,
        },
    },
    messages: {
        add_cash_input: {
            required: "Please enter Cash",
        },
        cash_expiry: {
            required: "Please enter Cash Expiry Date",
        },
    },
    submitHandler: function (form) {
        var url = "{{url('admin/cash')}}";
        var method = "post"

        /*if($('#bank_account_id').val()!="" && $('#bank_account_id').val()!=0){
            var url = url+"/"+$('#bank_account_id').val();
            var method = "put";
        } */

        $.ajax({
            type: method,
            url: url,
            data: $(form).serialize(),
            beforeSend: function () {
            },
            success: function (result)
            {
                result = JSON.parse(result)
                if(result.msg == 'Success')
                    toastr.success('Added Successfully',result.message)
                else
                    toastr.error('Added Successfully',result.message)
                $('#cashAdd').modal('hide');
            },
            error: function (error) {
                $('#cashAdd').modal('hide');
              //  toastr.error(result.message)
            }
        });
        return false;
    }
});
});
   
</script>
@endpush