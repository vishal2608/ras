@extends('layouts.backend')

{{-- @section('title',trans('customer.transactionHistory_per_company'))
@section('pageTitle',trans('customer.transactionHistory_per_company')) --}}
@section('title',trans('customer.transaction_summary_per_supplier'))
@section('pageTitle',trans('customer.transaction_summary_per_supplier'))


@section('content')
    <div class="row">

        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="box bordered-box blue-border">
                    <div class="box-header blue-background">
                        <div class="title">
                            <i class="icon-circle-blank"></i>
                            @lang('customer.transaction_summary_per_supplier')
                        </div>
                    </div>
                </div>
                
                <div class="panel-body">
                      
                    <div class="row">

                            <div class="col-md-12">
                                    <a href="{{ URL::previous() }}" title="Back">
                                            <button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> @lang('customer.back')
                                            </button>
                                        </a><br>
                                    </div>    

                        <div class="box-content">
                                
                            <div class="col-md-7">
                                <label>@lang('customer.customer') :</label>
                                <span> {{$customer->name}} </span><br>
                                <label>@lang('customer.main_contact_at_cb') :</label>
                                <span> 
                                    @if(isset($customer->user))
                                        {{$customer->user[0]->name}} 
                                    @endif 
                                </span><br>
                                <label>@lang('customer.phone') :</label>
                                <span> 
                                    @if(isset($customer->user))
                                        {{$customer->user[0]->phone}}
                                    @endif 
                                </span><br>
                                <label >@lang('customer.mobile') :</label>
                                <span> 
                                    @if(isset($customer->user))
                                        {{$customer->user[0]->mobile}}
                                    @endif
                                </span><br>
                                <br>
                                <label >@lang('customer.period') :</label>
                                <input type="text" name="startdate" value="{{$startdate}}" id="startdate" /> 
                                <label>@lang('customer.to') :</label>
                                <input type="text" name="enddate" id="enddate" value="{{$enddate}}" /> 
                                {!! Form::button('<i class="fa fa-floppy-o" aria-hidden="true"></i> Submit', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-success btn-xs',
                                    'title' => 'Change Period',
                                    'id' => 'changedate'
                                    
                                ))!!}
                                <br/>
                                <br/>
                                {{-- <label >@lang('customer.search')</label>
                                <input type="text" name="searchText" id="searchText" value="" />  --}}
                                 <br/>
                                <br/>
                                
                            </div>

                            <div class="col-md-5">
                                <label>@lang('customer.cash_made_available_for_anticipation') : </label>
                                <span id="cash_anticipation">
                                    @if(isset($customer->cash_anticipation) && count($customer->cash_anticipation) > 0)
                                        {{$customer->cash_anticipation[0]->anticipation}}
                                    @else 0.00
                                    @endif
                                </span><br>
                                <label>@lang('customer.anticipations_done') :</label>
                                <span>
                                    @if(isset($customer->anticipation_to_be_done))  
                                        @if(count($customer->anticipation_to_be_done) > 0 && $customer->anticipation_to_be_done[0]->total_done_paid != '')
                                            {{$customer->anticipation_to_be_done[0]->total_done_paid}}
                                        @else
                                            0.00
                                        @endif
                                    @else 0.00
                                    @endif
                                </span><br>
                                <label>@lang('customer.anticipations_to_be_paid') :</label>
                                <span> 
                                    @if(isset($customer->anticipation_to_be_paid_approved_today))
                                        @if(count($customer->anticipation_to_be_paid_approved_today) > 0 && $customer->anticipation_to_be_paid_approved_today[0]->total_to_paid != '')
                                            {{$customer->anticipation_to_be_paid_approved_today[0]->total_to_paid}}
                                        @else
                                            0.00
                                        @endif
                                    @else 0.00
                                    @endif 
                                </span><br>
                                <label>@lang('customer.cash_available') :</label>
                                <span id="cashAvailable">
                                    @if(isset($customer->cashAvailable) && count($customer->cashAvailable) > 0) 
                                        {{$customer->cashAvailable[0]->available_cash}}
                                    @else 0.00
                                    @endif
                                </span><br>
                                <label>@lang('customer.date_limit') : </label>
                                <span>
                                    @if(isset($customer->cashExpiry) && count($customer->cashExpiry) > 0)
                                        {{date('d/m/Y',strtotime($customer->cashExpiry[0]->created_at))}}
                                    @else Not Defined
                                    @endif 
                                </span><br>
                                <a href="#" class="btn btn-primary btn-xs" title="history" id="history" data-id="{{$customer->id}}"  >
                                @lang('customer.see_history')
                                </a>
                            </div>
                        

                        </div>

                    <div class="table-responsive">
                        <table id="customers-table" class="table table-borderless datatable responsive">
                            <thead>
                            <tr>
                                <th data-priority="1">@lang('customer.supplier')</th>
                                <th data-priority="2">@lang('customer.totalamount')</th>
                                <th data-priority="3">@lang('customer.anticipateamount')</th>
                                <th data-priority="4">@lang('customer.anticipations_transaction')</th>
                                {{-- <th data-priority="4">@lang('customer.transactionqty')</th> --}}
                                <th data-priority="5">@lang('customer.cessionamount')</th>
                                <th data-priority="6">@lang('customer.cessiontransaction')</th>
                                <th data-priority="7">@lang('customer.meanrate')</th>
                                <th data-priority="8">@lang('customer.revenueanticipation')</th>
                                <th data-priority="9">@lang('customer.revenuecession')</th>
                                <th data-priority="10">@lang('customer.totalfinalrevenue')</th>
                            </tr>
                            </thead>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>


    @include("admin.models.cash_history")


@endsection
@push('script-head')
<script> 
var url = '{{url('admin/transactions/view/')}}';
        datatable = $('#customers-table').DataTable({
            processing: true,
            serverSide: true,
            "bFilter": false,
            ajax: 
            {
                url: '{!! route('CustomerControllerTransactionHistoryData') !!}', // json datasource
                type: "get", // method , by default get
                data: function (d) {
                    d.customer_id = '{{$id}}';
                    d.startdate = $('#startdate').val();
                    d.enddate = $('#enddate').val();
                    d.searchText = $('#searchText').val();
                }
            },
                columns: [
                    {
                        'data':null,
                        "searchable": false,
                        render : function(o){
                            return o.name;
                        }
                    },
                    {data:'amounttotal',"searchable": false,
                        "orderable": false,name:'amounttotal'},
                    {data:'anticipationtotal',"searchable": false,
                        "orderable": false,name:'anticipationtotal'},
                    {data:'totalInvoiceAnticipation',"searchable": false,
                        "orderable": false,name:'totalInvoiceAnticipation'},    
                    // {
                    //     "data":null,
                    //     "searchable": false,
                    //     "orderable": false,
                    //     render : function(o){
                    //         return "<a href='"+url+"/"+o.id+"'>"+o.totalInvoiceAnticipation+"</a>";
                    //     }
                    // },
                    {data:'cessiontotal',"searchable": false,
                        "orderable": false,name:'cessiontotal'},
                    {data:'totalInvoiceCession',"searchable": false,
                        "orderable": false,name:'totalInvoiceCession'},
                    {
                        "data":null,
                        "searchable": false,
                        "orderable": false,
                        render: function(o){
							// if(o.totalInvoice != 0)
							// 	return o.rate/o.totalInvoice + " %";
							// else
							// 	return '-';
                            return o.mean_rate + " %";
                        }
                    },
                    {
                        "data":null,
                        "searchable": false,
                        "orderable": false,
                        render: function(o){
                            return o.revenuewForAnticipation;
                        }
                    },
                    {
                        "data":null,
                        "searchable": false,
                        "orderable": false,
                        render: function(o){
                            return o.revenuewForCession;
                        }
                    },
                    {
                        "data":null,
                        "searchable": false,
                        "orderable": false,
                        render: function(o){
                            return o.revenuewForAnticipation + +o.revenuewForCession;
                        }
                    }

                ]
        });

    
    $(document).on('click', '.del-item', function (e) {
        var id = $(this).attr('data-id');
        url = url + "/" + id;
        var r = confirm("Are you sure you want to delete Customer ?");
        if (r == true) {
            $.ajax({
                type: "delete",
                url: url ,
                headers: {
                    "X-CSRF-TOKEN": "<?php echo csrf_token();?>"
                },
                success: function (data) {
                    datatable.draw();
                    toastr.success('Action Success!', data.message)
                },
                error: function (xhr, status, error) {
                    var erro = ajaxError(xhr, status, error);
                    toastr.error('Action Not Procede!',erro)
                }
            });
        }
    });
    $('#changedate').click(function(){
        $.ajax({
                type: "post",
                url: '{{url('admin/customer/changeperiod/')}}' ,
                headers: {
                    "X-CSRF-TOKEN": "<?php echo csrf_token();?>"
                },
                data: {
                    customer_id : '{{$id}}',
                    startdate : $('#startdate').val(),
                    enddate : $('#enddate').val()
                },
                success: function (data) {
                    $("#cash_anticipation").html(data.cashavailableforanticipation);
                    $("#cashAvailable").html(data.cashavailable);
                    datatable.draw();
                },
                error: function (xhr, status, error) {
                    var erro = ajaxError(xhr, status, error);
                    toastr.error('Action Not Procede!',erro)
                }
            });
        
    });
  /*  $(document).on('keypress', '#searchText', function (e) {
        datatable.draw();
    }); */
    $(document).on('focusout', '#searchText', function (e) {
        datatable.draw();
    });
    $(function () {
        $('#startdate').datetimepicker({
            format: 'DD/MM/YYYY',
            //defaultDate : "{{$startdate}}",
        });
        $('#enddate').datetimepicker({
            format: 'DD/MM/YYYY',
            //defaultDate : "{{$enddate}}",
        });
        $("#startdate").on("dp.change", function (e) {
            format: 'DD/MM/YYYY',
            //defaultDate : "{{$enddate}}",
            $('#enddate').data("DateTimePicker").minDate(e.date);
        });
        $("#enddate").on("dp.change", function (e) {
            format: 'DD/MM/YYYY',
            $('#startdate').data("DateTimePicker").maxDate(e.date);
        });
    });


    /* Cash History Datatable*/

    $(document).on('click', '#history', function (e) {
        var id = $(this).attr('data-id');
        var balance = 0.0;
        $('#cash_history').modal('show');


        datatable = $('#cash-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: 
            {
                url: '{!! route('CustomerControllerCashHistoryData') !!}',
                type: "get", // method , by default get
                data: function (d) {
                    d.customer_id = id;

                }
                
            },

                columns: [
                    {
                        "data":null,
                        "searchable": false,
                        "orderable": false,
                        render: function(o){
                            if(o.supplier_id){
                                return o.supplier.name;
                            }else{
                                return '-';
                            }
                        }
                    },
                    {data:'action',name:'action'},
                    {data:'date',name:'date'},
                    {
                        "data":null,
                        "searchable": false,
                        "orderable": false,
                        render: function(o){
                            if(o.sales_user){
                                return o.sales_user;
                            }else{
                                return '-';
                            }
                        }
                    },
                    {data:'amount',name:'amount'},
                    {
                        
                        "data":null,
                        "searchable": false,
                        "orderable": false,
                        render: function(o){
                            
                            if(o.amount){
                              var first = o.amount;
                             balance = first + balance;
                             return balance;
                            }else{
                                return '-';
                            }

                        }
                    },


                ]
        });

    });

        

</script>
@endpush
