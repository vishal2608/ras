@extends('layouts.backend')

@section('title',trans('supplier.supplier'))
@section('pageTitle',trans('supplier.supplier'))

@section('content')
        <div class="row">
            <div class="col-md-12">
             <div class="box bordered-box blue-border">
                                          <div class="box-header blue-background">
                                                              <div class="title">
                                                                  <i class="icon-circle-blank"></i>
                                                               @lang('supplier.supplier') 
                                                              </div>

                                           </div>
                                           <div class="box-content ">

                        <a href="{{ URL::previous() }}"title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> @lang('supplier.back')</button></a>

                        @if(Auth::user()->can('access.suppliers.edit'))
                            <a href="{{ url('/admin/suppliers/' . $supplier->id . '/edit') }}" title="Edit Supplier"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> @lang('supplier.edit')</button></a>
                        @endif

                        @if(Auth::user()->can('access.suppliers.delete'))
                            {!! Form::open([
                                'method'=>'DELETE',
                                'url' => ['admin/suppliers', $supplier->id],
                                'style' => 'display:inline'
                            ]) !!}
                                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                        'type' => 'submit',
                                        'class' => 'btn btn-danger btn-xs',
                                        'title' => 'Delete Supplier',
                                        'onclick'=>'return confirm("Confirm delete?")'
                                ))!!}
                            {!! Form::close() !!}
                        @endif

                        @if(Auth::user()->can('access.transaction.history.supplier '))
                            <a href="{{ url('admin/transactions/view') }}/{{ $supplier->id }}" title="Check Transaction"><button class="btn btn-primary btn-xs"><i class="fa fa-info-circle" aria-hidden="true"></i> @lang('supplier.transaction')</button></a>
                        @endif
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <tbody>
                                    <tr>
                                        <th>@lang('supplier.id')</th><td>{{ $supplier->id }}</td>
                                    </tr>
                                    <tr>
                                        <th>@lang('supplier.name')</th><td> {{ $supplier->name }} </td>
                                    </tr>
                                    <tr>
                                        <th>@lang('supplier.cpnj')</th><td> {{ $supplier->cpnj }} </td>
                                    </tr>
                                    <tr>
                                        <th>@lang('supplier.website')</th><td> {{ $supplier->website }} </td>
                                    </tr>
                                    <tr>
                                        <th>@lang('supplier.legal')</th><td> {{ $supplier->legalname }} </td>
                                    </tr>
                                    <tr>
                                        <th>@lang('supplier.neighborhood')</th><td> {{ $supplier->neighborhood }} </td>
                                    </tr>
                                    <tr>
                                        <th>@lang('supplier.address')</th><td> {{ $supplier->address_line1 }} , {{ $supplier->address_line2 }} ,{{ $supplier->address_complement }} </td>
                                    </tr>
                                    
                                    <tr>
                                        <th> @lang('supplier.country')</th><td> {{ $supplier->country_name }} </td>
                                    </tr>
                                    <tr>
                                        <th> @lang('supplier.state')</th><td> {{ $supplier->state }} </td>
                                    </tr>
                                    <tr>
                                        <th> @lang('supplier.city')</th><td> {{ $supplier->city }} </td>
                                    </tr>
                                    <tr>
                                        <th>@lang('supplier.zip')</th><td> {{ $supplier->zip }} </td>
                                    </tr>

                                    {{--  <tr>
                                        <th> @lang('supplier.operator')</th><td> {{ $supplier_user['SLO'] }} </td>
                                    </tr>
                                    <tr>
                                        <th> @lang('supplier.approver')</th><td> {{ $supplier_user['SLO'] }} </td>
                                    </tr>  --}}

                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
@endsection
