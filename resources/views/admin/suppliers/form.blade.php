<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
    {!! Form::label('name',trans('supplier.supplier_group_name'), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('typeofsupplier') ? 'has-error' : ''}}">
        {!! Form::label('typeofsupplier',trans('supplier.type'), ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            {!! Form::select('typeofsupplier',array(''=>'Select','individual'=>'Individual','company'=>'Company'),(isset($supplier->typeofsupplier) && $supplier->typeofsupplier != '' )? $supplier->typeofsupplier : null,['class' => 'form-control selectTag']) !!}
            {!! $errors->first('typeofsupplier', '<p class="help-block">:message</p>') !!}
        </div>
</div>
<div class="form-group {{ $errors->has('website') ? 'has-error' : ''}}">
        {!! Form::label('website',  trans('supplier.website'), ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            {!! Form::text('website', null, ['class' => 'form-control', 'required' => 'required']) !!}
            {!! $errors->first('website', '<p class="help-block">:message</p>') !!}
        </div>
</div>

<div class="form-group {{ $errors->has('cb_rate_class') ? 'has-error' : ''}}">
        {!! Form::label('cb_rate_class',  trans('supplier.cb_rate_class'), ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            <select name="cb_rate_class" class="form-control" required id="SelectClass" >
                @foreach($discountRate as $rate)
                    <option value="{{$rate->id}}" 
                        <?php 
                        if(isset($discount_rate))
                            if($rate->id == $discount_rate->id) echo 'selected="selected"';
                        ?>
                        >    {{$rate->class}} ( {{$rate->min}}% to {{$rate->max}}% )</option>
                @endforeach
            </select>
            {!! $errors->first('cb_rate_class', '<p class="help-block">:message</p>') !!}
        </div>
    </div>


<div class="form-group {{ $errors->has('image') ? 'has-error' : ''}}">
    {!! Form::label('image', trans('supplier.supplier_image'), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-3">
    {!! Form::file('image', null, ['class' => 'form-control', 'required' => 'required']) !!}
    {!! $errors->first('image', '<p class="help-block with-errors">:message</p>') !!}
    </div>

    @if(isset($supplier) && $supplier->image)
    <div class="form-group ">
        <img src="{!! asset('images/'.$supplier->image) !!}" alt="image" width="50">
    </div>
    @endif

</div>



@if(Route::currentRouteName() == 'suppliers.create')

<div class="form-group {{ $errors->has('cbuser') ? 'has-error' : ''}}">
        {!! Form::label('cbuser',trans('supplier.cb_rep'), ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            
            @if(isset($commercialUsers) && $commercialUsers != "")
            {!! Form::select('cbuser',$commercialUsers,(isset($commercialSuppUsers) && $commercialSuppUsers != '' ) ? $commercialSuppUsers : '',['class' => 'form-control selectTag']) !!}
            {!! $errors->first('cbuser', '<p class="help-block">:message</p>') !!}
            @else
                No Active CB-Representatives
            @endif
        </div>
</div>
<input type="hidden" name="old_commercialUsers" value="<?php echo (isset($commercialSuppUsers) && $commercialSuppUsers != '' ) ? $commercialSuppUsers : ''; ?>" />

<div class="form-group">
        <div class="col-md-4 col-md-offset-5 text-center ad_bank_detail">
            {!! Form::label('label', 
             trans('supplier.add_unit_details')) !!}
        </div>
    </div>

<div class="form-group {{ $errors->has('unit_name') ? 'has-error' : ''}}">
            {!! Form::label('unit_name', trans('supplier.unit_name'), ['class' => 'col-md-4 control-label']) !!}
            <div class="col-md-6">
                {!! Form::text('unit_name', null, ['class' => 'form-control', 'required' => 'required']) !!}
                {!! $errors->first('unit_name', '<p class="help-block">:message</p>') !!}
            </div>
</div>

<div class="form-group {{ $errors->has('unit_legal_name') ? 'has-error' : ''}}">
        {!! Form::label('unit_legal_name', trans('supplier.unit_legal_name'), ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            {!! Form::text('unit_legal_name', null, ['class' => 'form-control', 'required' => 'required']) !!}
            {!! $errors->first('unit_legal_name', '<p class="help-block">:message</p>') !!}
        </div>
</div>

<div class="form-group {{ $errors->has('cpnj') ? 'has-error' : ''}}">
    {!! Form::label('cpnj', trans('supplier.cpnj'), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('cpnj', null, ['class' => 'form-control', 'required' => 'required','id'=>'cpf','placeholder' => '000.000.000-00']) !!}
        {!! $errors->first('cpnj', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('zip') ? 'has-error' : ''}}">
    {!! Form::label('zip', trans('supplier.zip'), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('zip', null, ['class' => 'form-control', 'required' => 'required','id' => 'zip','placeholder'=> '00000-000']) !!}
        {!! $errors->first('zip', '<p class="help-block">:message</p>') !!}
    </div>
</div>

{{--
<div class="form-group {{ $errors->has('address_street') ? 'has-error' : ''}}">
    {!! Form::label('address_street', trans('supplier.address_street'), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('address_street', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('address_street', '<p class="help-block">:message</p>') !!}
    </div>
</div>
--}}
<div class="form-group {{ $errors->has('address_street') ? 'has-error' : ''}}">
    {!! Form::label('address_street',trans('supplier.address_street'), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('address_street', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('address_street', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('address_number') ? 'has-error' : ''}}">
    {!! Form::label('address_number',trans('supplier.address_number') , ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('address_number', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('address_number', '<p class="help-block">:message</p>') !!}
    </div>
</div>



<div class="form-group {{ $errors->has('address_comlement') ? 'has-error' : ''}}">
    {!! Form::label('address_complement',trans('supplier.address_complement') , ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('address_complement', null, ['class' => 'form-control']) !!}
        {!! $errors->first('address_complement', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('neighborhood') ? 'has-error' : ''}}">
    {!! Form::label('neighborhood',trans('supplier.neighborhood'), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('neighborhood', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('neighborhood', '<p class="help-block">:message</p>') !!}
    </div>
</div>


<div class="form-group{{ $errors->has('countries') ? ' has-error' : ''}} ">
    {!! Form::label('countries', trans('supplier.country'), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6"> 
	 {!!Form::select('countries',$countries,'null',array('class'=>'form-control','id'=>'country'));!!}
       {!! $errors->first('countries', '<p class="help-block">:message</p>') !!}   
     </div>
</div>

<div class="form-group{{ $errors->has('state') ? ' has-error' : ''}}">
    
    {!! Form::label('state', trans('supplier.state'), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::select('state', ['' => 'Select State'],'null', ['class' => 'form-control  selectTag','id'=>'state']) !!}
        {!! $errors->first('state', '<p class="help-block">:message</p>') !!}

    </div>
</div> 



<div class="form-group {{ $errors->has('city') ? 'has-error' : ''}} ">
    {!! Form::label('city', trans('supplier.city'), ['class' => ' col-md-4 control-label']) !!}
    <div class="col-md-6">
       {!! Form::select('city', ['' => 'Select City'],'null', ['class' => 'form-control  selectTag','id'=>'city']) !!}
        {!! $errors->first('city', '<p class="help-block">:message</p>') !!}
    </div>
    
</div>

<div class="form-group">
    <div class="col-md-4 col-md-offset-5 text-center ad_bank_detail">
        {!! Form::label('label', 
         trans('supplier.add_bank_details')) !!}
    </div>
</div>

<div class="form-group {{ $errors->has('bank_name') ? 'has-error' : ''}}">
    {!! Form::label('bank_name', trans('supplier.bank_name'), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('bank_name', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('bank_name', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('bank_number') ? 'has-error' : ''}}">
    {!! Form::label('bank_number', trans('supplier.bank_number'), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('bank_number', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('bank_number', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('agency_name') ? 'has-error' : ''}}">
    {!! Form::label('agency_name', trans('supplier.agency_name'), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('agency_name', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('agency_name', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('agency_number') ? 'has-error' : ''}}">
    {!! Form::label('agency_number', trans('supplier.agency_number'), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('agency_number', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('agency_number', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('account_number') ? 'has-error' : ''}}">
    {!! Form::label('account_number', trans('supplier.account_number'), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('account_number', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('account_number', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('account_info') ? 'has-error' : ''}}">
    {!! Form::label('account_info', trans('supplier.account_info'), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('account_info', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('account_info', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('info') ? 'has-error' : ''}}">
    {!! Form::label('info', trans('supplier.information'), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::textarea('info', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('info', '<p class="help-block">:message</p>') !!}
    </div>
</div>
@endif



<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : trans('supplier.create'), ['class' => 'btn btn-primary', 'id' => 'SupplierCreate']) !!}
    </div>
</div>


@push('script-head')

<script type="text/javascript">
    $('#phone').mask('(00) 00000-0000');
    $('#cpf').mask('00.000.000/0000-00');
    $('#zip').mask('00000-000');
    $("#formSupplier").validate({
        //var cpf = $('#cpf').val();
        //cpf=cpf.replace(".","");
        //cpf = cpf.replace("-","");
        //cpf= cpf.replace(".","");
        //alert(cpf);
        rules: { 
            cpnj: {
                required: true,
                validateCpf: true
            }
        },
        messages: {
            cpnj: {
                required: "CPNJ is required",
                validateCpf: "Enter Valid CPNJ Code"
            }
        },
        submitHandler: function(form) {
            form.submit();
        }
    });
    $.validator.addMethod(
        "validateCpf", 
        function(value, element) {
            cnpj = value.replace(/[^\d]+/g,'');
 
    if(cnpj == '') return false;
     
    if (cnpj.length != 14)
        return false;
 
    // Elimina CNPJs invalidos conhecidos
    if (cnpj == "00000000000000" || 
        cnpj == "11111111111111" || 
        cnpj == "22222222222222" || 
        cnpj == "33333333333333" || 
        cnpj == "44444444444444" || 
        cnpj == "55555555555555" || 
        cnpj == "66666666666666" || 
        cnpj == "77777777777777" || 
        cnpj == "88888888888888" || 
        cnpj == "99999999999999")
        return false;
         
    // Valida DVs
    tamanho = cnpj.length - 2
    numeros = cnpj.substring(0,tamanho);
    digitos = cnpj.substring(tamanho);
    soma = 0;
    pos = tamanho - 7;
    for (i = tamanho; i >= 1; i--) {
      soma += numeros.charAt(tamanho - i) * pos--;
      if (pos < 2)
            pos = 9;
    }
    resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
    if (resultado != digitos.charAt(0))
        return false;
         
    tamanho = tamanho + 1;
    numeros = cnpj.substring(0,tamanho);
    soma = 0;
    pos = tamanho - 7;
    for (i = tamanho; i >= 1; i--) {
      soma += numeros.charAt(tamanho - i) * pos--;
      if (pos < 2)
            pos = 9;
    }
    resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
    if (resultado != digitos.charAt(1))
          return false;
           
    return true;
            
        },
        "Enter Valid CPNJ Code"
    );
    
</script>
@endpush