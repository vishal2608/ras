@extends('layouts.backend')

@section('title',trans('supplier.supplier'))
@section('pageTitle',trans('supplier.create_new_supplier'))

@section('content')
        <div class="row">
            <div class="col-md-12">
              <div class="box bordered-box blue-border">
                      <div class="box-header blue-background">
                          <div class="title">
                              <i class="icon-circle-blank"></i>
                              @lang('supplier.create_new_supplier')
                          </div>

                      </div>
                      <div class="box-content ">




                        <a href="{{ url('/admin/suppliers') }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> @lang('supplier.back')</button></a>
                        <br />
                        <br />

                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        {!! Form::open(['url' => '/admin/suppliers', 'class' => 'form-horizontal', 'files' => true,'id'=> 'formSupplier','enctype'=>'multipart/form-data']) !!}

                        @include ('admin.suppliers.form')

                        {!! Form::close() !!}




                    </div>
                </div>
            </div>
        </div>
@endsection

@push('script-head')
<script type="text/javascript">
    $('#country').change(function(){
    var countryID = $(this).val();    
    if(countryID){
        $.ajax({
           type:"GET",
           url:"{{url('admin/api/get-state-list')}}?country_id="+countryID,
           success:function(res){               
            if(res){
                $("#state").empty();
                $("#state").append('<option>Select</option>');
                $.each(res,function(key,value){
                    $("#state").append('<option value="'+key+'">'+value+'</option>');
                });
           
            }else{
               $("#state").empty();
            }
           }
        });
    }else{
        $("#state").empty();
        $("#city").empty();
    }      
   });
    $('#state').on('change',function(){
    var stateID = $(this).val();    
    if(stateID){
        $.ajax({
           type:"GET",
           url:"{{url('admin/api/get-city-list')}}?state_id="+stateID,
           success:function(res){               
            if(res){
                $("#city").empty();
                $.each(res,function(key,value){
                    $("#city").append('<option value="'+key+'">'+value+'</option>');
                });
           
            }else{
               $("#city").empty();
            }
           }
        });
    }else{ 
        $("#city").empty();
    }
        
   });
</script>
@endpush
