@extends('layouts.backend')

@section('title',trans('discount.discount_rate'))
@section('pageTitle',trans('discount.discount_rate'))

@section('content')
        <div class="row">
            <div class="col-md-12">
              <div class="box bordered-box blue-border">
                              <div class="box-header blue-background">
                                                  <div class="title">
                                                      <i class="icon-circle-blank"></i>
                                                     @lang('discount.discount_rate')
                                                  </div>

                               </div>
                               <div class="box-content ">

                        @if(isset($customer_id) && $customer_id != '')

                            <a href="{{ url('admin/customer ') }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> @lang('discount.back')</button></a>

                            @if(Auth::user()->can('create.discount.rate'))
                                <a href="{{url('/admin/discount-rate/create')}}?customer_id={{$customer_id}}" class="btn btn-success btn-sm" title="Add New DiscountRate"><i class="fa fa-plus" aria-hidden="true"></i> @lang('discount.add_new')</a>
                            @endif

                        @else
                            @if(Auth::user()->can('create.discount.rate'))
                                <a href="{{url('/admin/discount-rate/create')}}" class="btn btn-success btn-sm" title="Add New DiscountRate"><i class="fa fa-plus" aria-hidden="true"></i> @lang('discount.add_new')</a>
                            @endif
                        @endif
                        
                       


                        <br/>
                        <br/>
                        @if(isset($customer))
                            Discount Rate Class for Customer : {{$customer->name}}
                        @endif

                        <div class="table-responsive">
                            <table class="table table-borderless datatable responsive" id="discount-table">
                                <thead>
                                    <tr>
                                        <th data-priority="1">@lang('discount.id')</th>
                                        <th data-priority="2">@lang('discount.class')</th>
                                        <th data-priority="3">@lang('discount.minimum')</th>
                                        <th data-priority="4">@lang('discount.maximum')</th>
                                        <th data-priority="5">@lang('discount.actions')</th>
                                    </tr>
                                </thead>
                                
                            </table>
                            
                        </div>

                    </div>
                </div>
            </div>
        </div>
@endsection

@push('script-head')
<script>
var url ="{{ url('/admin/discount-rate/') }}";

        //Permissions
        var edit = "<?php echo Auth::user()->can('edit.discount.rate'); ?>";
        var discount_delete = "<?php echo Auth::user()->can('delete.discount.rate'); ?>";
        
        datatable = $('#discount-table').DataTable({
            processing: true,
            serverSide: true,
            "order": [[ 0, "desc" ]],
            ajax: {
                url: '{!! route('DiscountControllerDiscountsData') !!}',
                type: "get",
                data: function (d) {

                    d.customer_id = "{{$customer_id}}";

                }
            },
            // ajax: '{!! route('DiscountControllerDiscountsData') !!}',
                columns: [
                    { data: 'id', name: 'Id',"searchable": false },
                    { data: 'class', name: 'class' },
                    { 
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            var min= o.min;
                            return min.toFixed(2) + '%';
                        }
                    },
                     { 
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            var max= o.max;
                            return max.toFixed(2) + '%';
                        }
                    },
                    { 
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            var e=""; var d="";
                            
                            if(edit){
                                e= "<a href='"+url+"/"+o.id+"/edit' data-id="+o.id+"><button class='btn btn-primary btn-xs'><i class='fa fa-pencil-square-o' aria-hidden='true'></i></button></a>&nbsp;";
                            }
                            
                            if(discount_delete){
                                d = "<a href='javascript:void(0);' class='del-item' data-id="+o.id+" ><button class='btn btn-danger btn-xs'><i class='fa fa-trash-o' aria-hidden='true'></i></button></a>&nbsp;";
                            }
                            var v =  "<a href='"+url+"/"+o.id+"' data-id="+o.id+"><button class='btn btn-warning btn-xs'><i class='fa fa-eye' aria-hidden='true'></i>View</button></a>&nbsp;";

                            return v+e+d;
                        }
                    }
                ]
        });
    
    $(document).on('click', '.del-item', function (e) {
        var id = $(this).attr('data-id');
        url = url + "/" + id;
        var r = confirm("Are you sure you want to delete Discount Rate ?");
        if (r == true) {
            $.ajax({
                type: "delete",
                url: url ,
                headers: {
                    "X-CSRF-TOKEN": "<?php echo csrf_token();?>"
                },
                success: function (data) {
                    datatable.draw();
                    toastr.success('Action Success!', data.message)
                },
                error: function (xhr, status, error) {
                    var erro = ajaxError(xhr, status, error);
                    toastr.error('Action Not Procede!',erro)
                }
            });
        }
    });
</script>
@endpush

