@extends('layouts.backend')

@section('title',trans('bank.banks'))
@section('pageTitle',trans('bank.banks'))

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box bordered-box blue-border">
            <div class="box-header blue-background">
                <div class="title">
                    <i class="icon-circle-blank"></i>
                        @lang('bank.summary')
                </div>
            </div>
            <div class="box-content ">
                <br/>
                <br/>
                <div class="table-responsive">
                    <table class="table table-borderless datatable responsive" id="summary-table">
                        <thead>
                            <tr>
                                <th>@lang('bank.bank_name')</th>
                                <th>@lang('bank.noofcompany')</th>
                                <th>@lang('bank.cashtolend')</th>
                                <th>@lang('bank.amount_let')</th>
                                <th>@lang('bank.qty_of_invoice')</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('script-head')
<script>

var url ="{{ url('admin/summary-data/') }}";

        
        datatable = $('#summary-table').DataTable({
            processing: true,
            serverSide: true,
             ajax: url,
                columns: [
                    { data: 'name', name: 'name',"searchable": true },
                    {   'data': null ,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            if(o.customer_count.length > 0){
                               return o.customer_count[0].customercount  
                            }
                            return 0;
                        }
                    },
                    {   'data': null ,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            if(o.used_money.length > 0 && o.given_money.length > 0){
                                return o.given_money[0].totalamount - o.used_money[0].totalamount    
                            }
                            if(o.given_money.length > 0){
                               return o.given_money[0].totalamount  
                            }
                            return 0;
                            
                        }
                    },
                    {   'data': null ,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            if(o.used_money.length > 0){
                               return o.used_money[0].totalamount  
                            }
                            return 0;
                        }
                    },
                    {   'data': null ,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            if(o.invoice_qty.length > 0)
                                return o.invoice_qty[0].invoice_qty
                            return 0
                        }
                    }
                ]
        });
    
    $(document).on('click', '.del-item', function (e) {
        var id = $(this).attr('data-id');
        url = url + "/" + id;
        var r = confirm("Are you sure you want to delete Bank ?");
        if (r == true) {
            $.ajax({
                type: "delete",
                url: url ,
                headers: {
                    "X-CSRF-TOKEN": "<?php echo csrf_token();?>"
                },
                success: function (data) {
                    datatable.draw();
                    toastr.success('Action Success!', data.message)
                },
                error: function (xhr, status, error) {
                    var erro = ajaxError(xhr, status, error);
                    toastr.error('Action Not Procede!',erro)
                }
            });
        }
    });
</script>
@endpush


