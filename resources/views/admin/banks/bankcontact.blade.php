@extends('layouts.backend')

@section('title',trans('bank.bank_contacts'))
@section('pageTitle',trans('bank.bank_contacts'))

@section('content')
        <div class="row">
            <div class="col-md-12">
               <div class="box bordered-box blue-border">
                   <div class="box-header blue-background">
                                       <div class="title">
                                           <i class="icon-circle-blank"></i>
                                          @lang('bank.bank_contacts')
                                       </div>

                    </div>
                    <div class="box-content ">


                        <a href="{{ URL::previous() }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i>@lang('bank.back')</button></a>

                        @if(Auth::user()->can('add.bank.contacts'))
                        <a href="#" id="open_model_bank_contact_add" class="btn btn-success btn-xs" title="Add Bank Contacts">
                            <i class="fa fa-plus" aria-hidden="true"></i> @lang('bank.add_bank_contacts')
                        </a>
                        @endif

                        <br />
                        <br />

                         <div class="table-responsive">
                            <table class="table table-borderless datatable responsive" id="bankcontact-table">
                                <thead>
                                    <tr>
                                        <th data-priority="1">@lang('bank.id')</th>
                                        <th data-priority="2">@lang('bank.bank_name')</th>
                                        <th data-priority="3">@lang('bank.name')</th>
                                        <th data-priority="4">@lang('bank.phone')</th>
                                        <th data-priority="5">@lang('bank.mobile')</th>
                                        <th data-priority="6">@lang('bank.email')</th>
                                        <th data-priority="6">@lang('bank.status')</th>
                                        <th data-priority="7">@lang('bank.actions')</th>

                                    </tr>
                                </thead>

                            </table>

                        </div>

                    </div>
                </div>
            </div>
        </div>


        @include("admin.models.bank_contact")




@endsection

@push('script-head')
<script>

        var url ="{{ url('admin/bank/contact/datatable') }}/{{$bank->id}}";

        //Permissions
        var edit_contact = "<?php echo Auth::user()->can('edit.bank.contacts'); ?>";
        var delete_contact = "<?php echo Auth::user()->can('delete.bank.contacts'); ?>";
        
        datatable_c = $('#bankcontact-table').DataTable({
            processing: true,
            serverSide: true,
             ajax: url,
                columns: [
                    { data: 'id', name: 'Id',"searchable": false },
                    { data: 'bank.name',name:'bank.name'},
                    { data: 'name',name:'name'},
                    { data: 'phone', name: 'phone' },
                    { data: 'mobile', name: 'mobile' },
                    { data: 'email', name: 'email' },
                    { data: 'status', name: 'status' },
                    { 
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            var e="";var d="";
                            if(edit_contact){
                                e= "<a href='#' value="+o.id+" data-id="+o.id+" class='edit-item' ><button class='btn btn-primary btn-xs'><i class='fa fa-pencil-square-o' aria-hidden='true'></i>@lang('bank.edit')</button></a>&nbsp;";
                            }
                            if(delete_contact){
                                d = "<a href='javascript:void(0);' class='del-item' data-id="+o.id+" ><button class='btn btn-danger btn-xs'><i class='fa fa-trash-o' aria-hidden='true'></i> @lang('bank.delete')</button></a>&nbsp;";
                            }
                            return e+d;
                        }
                    
                    
                    }
                ]
        });

    $(document).on('click', '#open_model_bank_contact_add', function (e) {
        var formid = $("#bank_contact_form");
        resetupdateform(formid);
        $('#contacts').modal('show');
    });
    $(document).on('click', '.del-item', function (e) {
        var id = $(this).attr('data-id');
        url = "{{url('admin/bank/contact')}}/" + id;
        var r = confirm("Are you sure you want to delete Bank Contacts?");
        if (r == true) {
            $.ajax({
                type: "delete",
                url: url ,
                headers: {
                    "X-CSRF-TOKEN": "<?php echo csrf_token();?>"
                },
                success: function (data) {
                    datatable_c.draw();
                    toastr.success('Action Success!', data.message)
                },
                error: function (xhr, status, error) {
                    var erro = ajaxError(xhr, status, error);
                    toastr.error('Action Not Procede!',erro)
                }
            });
        }
    });


    $(document).on('click', '.edit-item', function (e) {

        var id = $(this).attr('data-id');

       var burl = "{{url('admin/bank/contact')}}/"+id+"/edit";
        $.get(burl, function (result) {
            var data =result.data;
            $('#bank_contact_id').val(data.id);
            $('#bank_id').val(data.bank_id);
            $('#contact_name').val(data.name);
            $('#contact_phone').val(data.phone);
            $('#contact_mobile').val(data.mobile);
            $('#contact_email').val(data.email);
            $('#contact_comments').val(data.comments);
            if(data.status == "active")
                $('#contact_status_active').prop("checked", true);
            if(data.status == "inactive")
                $('#contact_status_inactive').prop("checked", true);
            $('.error').html("");
            $('.contact_form_submit_button').html("Update");
            $('#contact_form_model_lable').html("Edit Contact Detail");

            $('#contacts').modal('show');
        })
    });

    function resetupdateform(formid) {
        $(formid)[0].reset();
        $('#contact_status_active').prop("checked", false);
        $('#contact_status_inactive').prop("checked", false);
        $('.error').html("");
        $("#bank_contact_id").val("");
        $('#contact_form_model_lable').html("Add Bank Contact Detail");
        $(".contact_form_submit_button").html("Create");
    }

$("#bank_contact_form").validate({
    rules: {
        name: {
            required: true,
        },
        phone: {
            required: true,
        },
        mobile: {
            required: true,
            
        },
        email:{
            required: true,
            email:true
        },
        comments:{
            required: true,
        },
        status:{
            required: true,
        }
    },
    messages: {
        name: {
            required: "Please  name.",
        },
        phone: {
            required: "Please enter phone number",
        },
        mobile: {
            required: "Please enter Contact number",
        },
        email: {
            required: "Please enter address",
            email:"Please enter valid email address"
        },
        comments: {
            required: "Please enter Comments",
        },
        comments: {
            required: "Select Staus",
        },
    },

    submitHandler: function (form) {


        var url = "{{url('admin/bank/contact')}}";
        var method = "post"

        if($('#bank_contact_id').val()!="" && $('#bank_contact_id').val()!=0){
            var url = url+"/"+$('#bank_contact_id').val();
            var method = "put";

        }

        $.ajax({
            type: method,
            url: url,
            data: $(form).serialize(),
            beforeSend: function () {
            },
            success: function (result)
            {
                $('#contacts').modal('hide');
                datatable_c.draw();
                toastr.success(result.messages);

            },
            error: function (error) {
                $('#contacts').modal('hide');
                toastr.error(result.messages);
                

            }
        });
        return false;
    }
});

</script>
@endpush







