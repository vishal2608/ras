@extends('layouts.backend')


@section('title',trans('user.add_user'))



@section('pageTitle')
    <i class="icon-tint"></i>

    <span>@lang('user.add_new_user')</span>


    @endsection


@section('content')
        <div class="row">
            <div class="col-md-12">
                <div class="box bordered-box blue-border">
                <div class="box-header blue-background">
                    <div class="title">
                        <i class="icon-circle-blank"></i>
                            @lang('user.add_new_user')
                    </div>

                </div>
                <div class="box-content">
                
                        <a href="{{ URL::previous() }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i>@lang('user.back')</button></a>
                        <br />
                        <br />

                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        {!! Form::open(['url' => '/admin/users', 'class' => 'form-horizontal','id'=>'formUser','enctype'=>'multipart/form-data']) !!}

                        @include ('admin.users.form')

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
@endsection

@push('script-head')
<script !src="">
        $(function () {
            $("#role").change();
        });
    var customer_id = "{{$customer_id}}";
    var supplier_id = "{{$supplier_id}}";
    
    jQuery('#role').on('change', function () {

        var role = jQuery(this).val();
        if (role) {
            jQuery.ajax({
                type: "GET",
                url: "{{ url('/admin/users/create/') }}?role=" + role,
                success: function (res) {
                    if (res) {
                        
                        //console.log(role);
                        $("#role_related").prop('disabled', false);
                        jQuery("#role_related").empty();
                        jQuery.each(res, function (key, value) {

                            if(key == customer_id)
                                jQuery("#role_related").append('<option value="' + key + '" selected="selected">' + value + '</option>');
                            else if(key == supplier_id)
                                jQuery("#role_related").append('<option value="' + key + '" selected="selected">' + value + '</option>');
                            else
                                jQuery("#role_related").append('<option value="' + key + '">' + value + '</option>');
                        });

                    } else {
                        jQuery("#role_related").empty();
                    }
                }
            });
        } else {
            jQuery("#role_related").empty();
        }

    });
</script>
@endpush