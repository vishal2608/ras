
<div class="form-group{{ $errors->has('name') ? ' has-error' : ''}}">
    {!! Form::label('name', trans('user.name'), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('email') ? ' has-error' : ''}}">
    {!! Form::label('email',trans('user.email'), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::email('email', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('password') ? ' has-error' : ''}}">
    {!! Form::label('password',trans('user.password'), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::password('password', ['class' => 'form-control']) !!}
        {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('roles') ? ' has-error' : ''}}">
    {!! Form::label('role', trans('user.role'), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::select('roles', $roles, isset($user_roles) ? $user_roles : [], ['class' => 'form-control  selectTag', 'multiple' => false,'id'=>'role']) !!}

    </div>
</div>
@if(Route::currentRouteName() != 'users.edit'  )
<div class="form-group{{ $errors->has('related') ? ' has-error' : ''}}">
    {!! Form::label('related', trans('user.related_to'), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        <select id="role_related" class="form-control" name="related"  value="">
            <option value=""> </option>
        </select>

    </div>
</div>
@endif


@if(Route::currentRouteName() == 'users.edit'  )
<div class="form-group{{ $errors->has('related') ? ' has-error' : ''}}">
    {!! Form::label('related', trans('user.related_to'), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">


        <select class="form-control" data-preselected="" id="related" name="related"  value="" >
            @if(isset($user_roles) && $user_roles == 'CM')
                @foreach($user_customer as $key => $cust)
                    <option value="{{$key}}"  <?php if(count($user->customer) > 0 && $key == $user->customer[0]->id) echo "selected='selected'"; ?> >{{$cust}}</option>
                @endforeach
            @endif
            @if(isset($user_roles) && $user_roles == 'SLA' || isset($user_roles) && $user_roles == 'SLO')
                @foreach($user_supplier as $key => $sup)
                    <option value="{{$key}}"  <?php if(count($user->supplier) > 0 && $key == $user->supplier[0]->id) echo "selected='selected'"; ?> >{{$sup}}</option>
                @endforeach
            @endif 
            @if(isset($user_roles) && $user_roles == 'SU' || isset($user_roles) && $user_roles == 'CBA' || isset($user_roles) && $user_roles == 'CBB' || isset($user_roles) && $user_roles == 'CBC' || isset($user_roles) && $user_roles == 'CBS')
                @foreach($related as $relates)
                    <option value="{{$relates}}">{{$relates}}</option>
                @endforeach
            @endif 
        </select>

    </div>
</div>
@endif  

<div class="form-group{{ $errors->has('cpnj') ? ' has-error' : ''}}">
    {!! Form::label('cpnj', trans('user.cpf'), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('cpnj', null, ['class' => 'form-control', 'required' => 'required','id'=>'cpf','placeholder' => '000.000.000-00']) !!}
        {!! $errors->first('cpnj', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('mobile') ? ' has-error' : ''}}">
    {!! Form::label('mobile', trans('user.mobile'), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('mobile', null, ['class' => 'form-control', 'required' => 'required','id' => 'mobile','placeholder'=>'(00) 00000-0000']) !!}
        {!! $errors->first('mobile', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('phone') ? ' has-error' : ''}}">
    {!! Form::label('phone', trans('user.phone'), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('phone', null, ['class' => 'form-control', 'required' => 'required','id' => 'phone','placeholder'=>'(00) 0000-0000']) !!}
        {!! $errors->first('phone', '<p class="help-block">:message</p>') !!}
    </div>
</div>


<div class="form-group {{ $errors->has('image') ? 'has-error' : ''}}">
    {!! Form::label('image', trans('user.user_image'), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-3">
    {!! Form::file('image', null, ['class' => 'form-control', 'required' => 'required']) !!}
    {!! $errors->first('image', '<p class="help-block with-errors">:message</p>') !!}
    </div>

    @if(isset($user) && $user->image)
    <div class="form-group ">
        <img src="{!! asset('images/'.$user->image) !!}" alt="image" width="50">
    </div>
    @endif

</div>

<div class="form-group{{ $errors->has('dob') ? ' has-error' : ''}}">
    {!! Form::label('dob', trans('user.dob'), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('dob', null, ['class' => 'form-control', 'placeholder' => 'DD/MM/YYYY','required' => 'required','id' => 'dob']) !!}
        {!! $errors->first('dob', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group{{ $errors->has('comments') ? ' has-error' : ''}}">
    {!! Form::label('comments', trans('user.comments'), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::textarea('comments', null, ['class' => 'form-control']) !!}
        {!! $errors->first('comments', '<p class="help-block">:message</p>') !!}
    </div>
</div>



<div class="form-group{{ $errors->has('user_status') ? ' has-error' : ''}}">
    {!! Form::label('status', trans('user.status'), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-3">
        {!! Form::label('active', trans('user.active'), ['class' => ' control-label']) !!}
        {!! Form::radio('user_status', 'active') !!}
       
    </div>
    <div class="col-md-3">
            {!! Form::label('inactive', trans('user.inactive'), ['class' => 'control-label']) !!}
            {!! Form::radio('user_status', 'inactive') !!}
            
    </div>
    {!! $errors->first('user_status', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : trans('user.create'), ['class' => 'btn btn-primary']) !!}
    </div>
</div>

@push('script-head')

<script type="text/javascript">
    $('#phone').mask('(00) 0000-0000');
    $('#mobile').mask('(00) 00000-0000');
    $('#cpf').mask('000.000.000-00');
    $("#formUser").validate({
        //var cpf = $('#cpf').val();
        //cpf=cpf.replace(".","");
        //cpf = cpf.replace("-","");
        //cpf= cpf.replace(".","");
        //alert(cpf);
        rules: { 
            cpnj: {
                required: true,
                validateCpf: true
            },
            name:{
                required:true
            },
            roles:{
                required:true
            },
            related:{
                required:true
            },
            email:{
                required:true,
                email: true
            },
            password:{
             //    required:true,
             //   pwcheck: true
            },
            mobile:{
                required:true,
                //minlength:10,
                //maxlength:11,
                //number: true
            },
            phone:{
                required:true
            },
            dob:{
                required:true
            },
            user_status:{
                required:true
            },
        },
        messages: {
            cpnj: {
                required: "CPF is required",
                validateCpf: "Enter Valid Cpf Code"
            },
            name:{
                required:"Name Is required"
            },
            
            roles:{
                required:"Select the role"
            },
            related:{
                required:"Select the relation according to role"
            },
            email:{
                required:"Email is required",
                email: "Enter Valid Email address"
            },
            password:{
               // required:"Password is required",
               // pwcheck: "Password must be contain 1 uppercase, 1 lowercase and 1 number value"
            },
            mobile:{
                required:"Mobile No is required",
               // minlength:"Must be contain minimum 10 digit",
                //maxlength:"Must be contain maximum 11 digit",
                //number: "Mobile No must be number"
            },
            phone:{
                required:"Phone no is required",
               // number:"Phone no is must be number"
            },
            dob:{
                required:"Enter Date of Birth"
            },
            user_status:{
                required:"Select the Status"
            },

        },
        submitHandler: function(form) {
            form.submit();
        }
    });
    $.validator.addMethod("pwcheck", function(value) {
        return /^[A-Za-z0-9\d=!\-@._*]*$/.test(value) // consists of only these
            && /[a-z]/.test(value) // has a lowercase letter
            && /\d/.test(value) // has a digit
    });
     $('#dob').datetimepicker({
        format: 'DD/MM/YYYY',
    });
    $.validator.addMethod(
        "validateCpf", 
        function(value, element) {
            value=value.replace(".","");
            value = value.replace("-","");
            value= value.replace(".","");
            var Sum;
            var Other;
            Sum = 0;
	        if (value == "00000000000") return false;
    
        	for (i = 1; i <= 9; i ++) Sum = Sum + parseInt (value.substring (i-1, i)) * (11 - i);
	        Rest = (Sum * 10)% 11;
	
            if ((Rest == 10) || (Rest == 11)) Rest = 0;
            if (Rest != parseInt (value.substring (9, 10))) return false;
	
        	Sum = 0;
            for (i = 1; i <= 10; i ++) Sum = Sum + parseInt (value.substring (i-1, i)) * (12 - i);
            Rest = (Sum * 10)% 11;
    
            if ((Rest == 10) || (Rest == 11)) Rest = 0;
            if (Rest != parseInt (value.substring (10, 11))) return false;
            return true;
            
        },
        "Enter Valid Cpf Code"
    );




</script>
@endpush

