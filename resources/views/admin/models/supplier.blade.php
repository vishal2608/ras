<!-- Supplier Modal -->
<div class="modal fade" id="supplierAddModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">@lang('supplier.add_supplier')</h4>
      </div>
      <div class="modal-body">

        {!! Form::open(['url' => '/admin/suppliers', 'id'=>'creatSupplier','class' => 'form-horizontal', 'files' => true]) !!}
        <div class="form-group has-error" id="errorSupplier"></div>
            @include('admin.suppliers.form')
        {!! Form::close() !!}
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">@lang('supplier.close')</button>
      </div>
    </div>
  </div>
</div>