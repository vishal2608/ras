<!-- Add Bank Account Modal -->
<div class="modal fade" id="accounts" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">

                <h5 class="modal-title" id="account_form_model_lable"> @lang('bank.add_bank_accounts_detail')</h5> 

                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="bidder_list">
                    <div class=" bidder no-padding-left no-padding-right gutter-bottom">
                        
                        <div class=" clearfix details-container details-port-container">
                            <form method="post" id="bank_account_form" name="form">
                <input  name="bank_id"  id="bank_id" type="hidden" value="{{$bank->id}}" >
                <input  name="bank_account_id" id="bank_account_id"  type="hidden" value="" >
                                {{csrf_field()}} 
                                <div class="form-group prepend-top">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label class="pull-left required" for="Projects_title">@lang('bank.account_number')</label>
                                            <input class="form-control form-control clear" name="account_number" id="account_number"   type="text" maxlength="85" value="" onkeypress="return isNumber(event)" >
                                        </div>
                                        <div class="col-md-6">
                                            <label class="pull-left required" for="Projects_title">@lang('bank.account_info')</label>
                                            <input class="form-control form-control clear" name="account_info" id="account_info" type="text" maxlength="85" value="">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group prepend-top">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label class="pull-left required" for="Projects_title">@lang('bank.agency_number')</label>
                                            <input class="form-control form-control clear" name="agency_number" id="agency_number" type="text" maxlength="85" value="" onkeypress="return isNumber(event)" >
                                        </div>
                                        <div class="col-md-6">
                                            <label class="pull-left required" for="Projects_title">@lang('bank.agency_name')</label>
                                            <input class="form-control form-control clear" name="agency_name" id="agency_name" type="text" maxlength="85" value="">
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="form-group prepend-top">
                                    <label class="pull-left required" for="Projects_proj_desc">@lang('bank.information')</label>
                                    <textarea class="form-control autoresized-textarea clear" rows="3" id="info" name="info" value=""></textarea>
                                </div>


                                <div class="form-group prepend-top">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label class="pull-left required" for="Projects_title">@lang('bank.street_name')</label>
                                            <input class="form-control form-control clear" name="street_name" id="street_name"   type="text" maxlength="85" value=""  >
                                        </div>
                                        <div class="col-md-6">
                                            <label class="pull-left required" for="Projects_title">@lang('bank.address_number')</label>
                                            <input class="form-control form-control clear" name="address_number" id="address_number" type="text" maxlength="85" value="">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group prepend-top">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label class="pull-left required" for="Projects_title">@lang('bank.complement')</label>
                                            <input class="form-control form-control clear" name="complement" id="complement" type="text" maxlength="85" value="" >
                                        </div>
                                        <div class="col-md-6">
                                            <label class="pull-left required" for="Projects_title">@lang('bank.neighborhood')</label>
                                            <input class="form-control form-control clear" name="neighborhood" id="neighborhood" type="text" maxlength="85" value="">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group prepend-top">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label class="pull-left required" for="Projects_title">@lang('bank.country')</label>
                                            <input type="hidden" name="country_id" id="country_id">
                                            
                                             {!!Form::select('country',$countries,'null',array('class'=>'form-control','id'=>'country'));!!}
       
                                        </div>
                                        <div class="col-md-6">
                                            <label class="pull-left required" for="Projects_title">@lang('bank.state')</label>

                                              {!! Form::select('state', ['' => 'Select State'],'null', ['class' => 'form-control  selectTag','id'=>'state']) !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group prepend-top">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label class="pull-left required" for="Projects_title">@lang('bank.city')</label>
                                            {!! Form::select('city', ['' => 'Select City'],'null', ['class' => 'form-control  selectTag','id'=>'city']) !!}
                                        </div>
                                        <div class="col-md-6">
                                            <label class="pull-left required" for="Projects_title">@lang('bank.zip')</label>
                                            <input class="form-control form-control clear" name="zip" id="zip" type="text" maxlength="8" value="" placeholder="00000-000" onkeypress="return isNumber(event)">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group prepend-top">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <button class="btn btn-read btn-inverted account_form_submit_button" type="submit" name="submit" value="account_form_submit_button">
                                                @lang('bank.create')
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@push('js')
<script>
$('#zip').mask('00000-000');
 function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

$('#country').change(function(){
    var countryID = $(this).val();    
    if(countryID){
        $.ajax({
           type:"GET",
           url:"{{url('admin/api/get-state-list')}}?country_id="+countryID,
           success:function(res){               
            if(res){
                $("#state").empty();
                $("#state").append('<option>Select</option>');
                $.each(res,function(key,value){
                    $("#state").append('<option value="'+key+'">'+value+'</option>');
                });
           
            }else{
               $("#state").empty();
            }
           }
        });
    }else{
        $("#state").empty();
        $("#city").empty();
    }      
   });
    $('#state').on('change',function(){
    var stateID = $(this).val();    
    if(stateID){
        $.ajax({
           type:"GET",
           url:"{{url('admin/api/get-city-list')}}?state_id="+stateID,
           success:function(res){               
            if(res){
                $("#city").empty();
                $.each(res,function(key,value){
                    $("#city").append('<option value="'+key+'">'+value+'</option>');
                });
           
            }else{
               $("#city").empty();
            }
           }
        });
    }else{
        $("#city").empty();
    }
        
   });

</script>
@endpush
<!-- Add Bank Account Modal Close-->
