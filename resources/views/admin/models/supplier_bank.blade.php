<!-- Add Supplier Bank Account Modal -->
<div class="modal fade" id="supplierbank" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="account_form_model_lable">@lang('supplier.add_supplier_bank_account')</h5> 
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="bidder_list">
                    <div class=" bidder no-padding-left no-padding-right gutter-bottom">
                        
                        <div class=" clearfix details-container details-port-container">
                            <form method="post" id="supplier_bank_account_form" name="form">
                <input  name="supplier_id"  id="supplier_id" type="hidden" value="{{$supplier->id}}" >
                <input  name="bank_account_id" id="bank_account_id"  type="hidden" value="" >
                                {{csrf_field()}} 
                                <div class="form-group prepend-top">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label class="pull-left required" for="Projects_title">@lang('supplier.bank_name')</label>
                                            <input class="form-control form-control clear" name="bank_name" id="bank_name"   type="text" maxlength="85" value="">
                                        </div>
                                        <div class="col-md-6">
                                            <label class="pull-left required" for="Projects_title">@lang('supplier.bank_number')</label>
                                            <input class="form-control form-control clear" name="bank_number" id="bank_number" type="text" maxlength="85" value="" onkeypress="return isNumber(event)" >
                                        </div>


                                    </div>
                                </div>
                                <div class="form-group prepend-top">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label class="pull-left required" for="Projects_title">@lang('supplier.agency_name')</label>
                                            <input class="form-control form-control clear" name="agency_name" id="agency_name" type="text" maxlength="85" value="">
                                        </div>
                                        <div class="col-md-6">
                                            <label class="pull-left required" for="Projects_title">@lang('supplier.agency_number')</label>
                                            <input class="form-control form-control clear" name="agency_number" id="agency_number" type="text" maxlength="85" value="">
                                        </div>
                                        
                                    </div>
                                </div>
                                <div class="form-group prepend-top">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label class="pull-left required" for="Projects_title">@lang('supplier.account_number')</label>
                                            <input class="form-control form-control clear" name="account_number" id="account_number"   type="text" maxlength="85" value="" onkeypress="return isNumber(event)">
                                        </div>
                                        <div class="col-md-6">
                                            <label class="pull-left required" for="Projects_title">@lang('supplier.account_info')</label>
                                            <input class="form-control form-control clear" name="account_info" id="account_info" type="text" maxlength="85" value="">
                                        </div>
                                    </div>
                                </div>
                                
                                
                                <div class="form-group prepend-top">
                                    <label class="pull-left required" for="Projects_proj_desc">@lang('supplier.information')</label>
                                    <textarea class="form-control autoresized-textarea clear" rows="7" id="info" name="info" value=""></textarea>
                                </div>
                                <div class="form-group prepend-top">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <button class="btn btn-read btn-inverted account_form_submit_button" type="submit" name="submit" value="account_form_submit_button">
                                                @lang('supplier.create')
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@push('js')
<script>
 function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

$('#account_number').mask("9999-9999-9999-9999");
$("#agency_number").mask("9999-9999-9999-9999");
/*$('#agency_number').keyup(function() {
  var foo = $(this).val().split("-").join(""); // remove hyphens
  if (foo.length > 0) {
    foo = foo.match(new RegExp('.{1,4}', 'g')).join("-");
  }
  $(this).val(foo);
});*/


</script>
@endpush
<!-- Add Supplier Bank Account Modal Close-->
