<!--  Cb - Commercial  Modal -->
<div class="modal fade" id="changeCbRepresentative" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title " id="account_form_model_lable">{{ trans('supplier.change_cb_rep') }} for <i id="supplierChangeCbRep"></i></h5> 
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="bidder_list">
                    <div class=" bidder no-padding-left no-padding-right gutter-bottom">
                        <div class=" clearfix details-container details-port-container">
                            <form method="post" id="change_cb_rep_form" name="form">
                                <input  name="cbsupplier_id"  id="cbsupplier_id" type="hidden" value="0" >
                                <input  name="oldcbrep_id"  id="oldcbrep_id" type="hidden" value="0" >
                                {{csrf_field()}} 
                                <div class="form-group prepend-top">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label class="pull-left required" for="Projects_title">@lang('supplier.cb_representative') : </label>
                                            
                                            <select id="cbcommercial" class="form-control" name="cbrepforsupplier">
                                                @foreach($user as $users)
                                                <option value="{{$users->id}}">{{$users->name}}</option>
                                                @endforeach
                                            </select>

                                        </div>
                                    </div>
                                </div>
                                <div class="form-group prepend-top">
                                    <div class="row
                                    ">
                                        <div class="col-md-4">
                                            <button class="btn btn-read btn-inverted account_form_submit_button" type="submit" name="submit" value="submit">
                                                @lang('supplier.change')
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <a id="YOUR_ID" href="#" ><button class='btn btn-warning btn-xs' id="log" onclick="myFunction()">@lang('supplier.logs')</button></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Cb- Commercial  Modal Close-->
@push('script-head')
<script>
    function myFunction(){
        var value = $('#cbsupplier_id').val();
       
        var url = "{{url('admin/suppliers')}}"+"/"+value+"/logs";
        
         var element = document.getElementById('YOUR_ID');
          element.setAttribute("href",url);
    }
        

</script>
@endpush